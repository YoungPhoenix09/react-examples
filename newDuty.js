import React from 'react'
import { Meteor } from 'meteor/meteor'
import './styles/newDuty.scss'

class NewDuty extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      isDutyValid: false,
      dutyName: "",
      dutyDesc: "",
      showErr: false,
      isProcessing: false,
      errMsg: ""
    }

    this.addDuty = this.addDuty.bind(this)
    this.closeButton = this.closeButton.bind(this)
    this.validateFields = this.validateFields.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  addDuty () {
    let me = this

    //Disable clicking
    me.setState({
      isProcessing: true
    })

    //Call insert
    Meteor.call("duty.add", {
      name: this.state.dutyName,
      desc: this.state.dutyDesc,
      adminId: this.props.admin._id
    }, (err, res) => {
      //Enable clicking
      me.setState({isProcessing: false})

      if (err || res) {
        //show error
        me.setState({
          showErr: true,
          errMsg: "Something went wrong while trying to add your duty.  Please try again later."
        })
      } else {
        //Update Success Handler
        this.props.dutyPage.setState({
          dutiesUpdated: true
        })

        //Close dialog
        this.closeButton()
      }
    })
  }

  handleInputChange () {
    let component = this
    let evtTarget = event.target

    if (evtTarget.id === "dutyName") {
      this.setState({dutyName: evtTarget.value.substr(0, 35)}, this.validateFields)
    } else {
      this.setState({dutyDesc: evtTarget.value.substr(0, 120)}, this.validateFields)
    }
  }

  validateFields () {
    let nameRegEx = /^[A-z0-9](\w| ){0,34}$/
    let descRegEx = /^.{0,120}$/

    if (this.state.dutyName.match(nameRegEx) !== null &&
        this.state.dutyDesc.match(descRegEx) !== null) {
      this.setState({isDutyValid: true})
    } else {
      this.setState({isDutyValid: false})
    }
  }

  closeButton () {
    if (!this.state.isProcessing) {
      //init state
      this.setState({
        isDutyValid: false,
        dutyName: "",
        dutyDesc: "",
        showSucccess: false,
        showErr: false,
        isProcessing: false
      })

      //Close modal
      this.props.close()
    }
  }

  render () {
    //Setup classes
    let hide = (this.props.showDialog) ? "w3-show " : "",
        waiting = (this.state.isProcessing) ? "" : "waiting ",
        charCount = "(" + (120 - this.state.dutyDesc.length) +
          " character" + ((this.state.dutyDesc.length !== 1) ? "s" : "") + " left)",
        //isNameInvalid = this.state.dutyName.match(/\W+/) !== null,
        isNameStartBad = this.state.dutyName.match(/^\W.*$/) !== null

    //Default view
    let view = (
      <div className="container">
        <div className="duty-fields">
          <div className="duty-name-cntnr">
            <label>Duty Name</label>
            <input  id="dutyName"
                    type="text"
                    className="duty-name-input"
                    value={this.state.dutyName}
                    onChange={this.handleInputChange}
            />
            <label className={((isNameStartBad) ? "validation" : "w3-hide")}>
              Must start with a letter or number!
            </label>
            <label className={((!!this.state.dutyName[0] && !this.state.isDutyValid && !isNameStartBad) ? "validation" : "w3-hide")}>
              Letters, numbers, spaces and underscores please!
            </label>
          </div>
          <div className="duty-desc-cntnr">
            <div className="duty-desc-hdr">
              <label>Short Description</label>
              <div className="char-count">{charCount}</div>
            </div>
            <textarea id="dutyDesc"
                      className="duty-desc-input"
                      value={this.state.dutyDesc}
                      onChange={this.handleInputChange}
            />
          </div>
        </div>
        <div className="control-bar">
          <button
            onClick={this.addDuty}
            className="primary w3-round-large"
            disabled={!this.state.isDutyValid || this.state.isProcessing}
          >Add Duty</button>
        </div>
      </div>
    )
    if (this.state.showErr) {
      view = (
        <div className="err-container">
          <h1>Whoops!</h1>
          <p>{this.state.errMsg}</p>
        </div>
      )
    }

    return (
      <div className={hide + waiting + "w3-modal new-duty-modal"}>
        <div className="w3-modal-content">
          <button
            onClick={this.closeButton}
            className="cls-button"
          >X</button>
          <div className="w3-container">
            <div className="w3-row-padding">
              {view}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default NewDuty
