import React from "react";
import ReactDOM from "react-dom";
import MyRostersList from "./myRostersList";
import MyRostersDetail from "./myRostersDetail";
import DialogBox from "./dialogBox";
import "./styles/myRostersView.css";

import Rosters from "../api/rosters";

class MyRostersView extends React.Component {
  constructor (props) {
    super(props);

    /*
    let rosters = [{
      id: 0,
      name: "Church",
      duties: [{
        id: 0,
        name: "Preaching",
        desc: "",
        assignedUsers: []
      }, {
        id: 1,
        name: "Song Leading",
        desc: "",
        assignedUsers: []
      }],
      users: [{
        id: 0,
        username: "",
        lastName: "Payne",
        firstName: "Jonathan",
        email: "",
        phone: "",
        assignableDuties: []
      },{
        id: 1,
        username: "",
        lastName: "Brevil",
        firstName: "Jerry",
        email: "",
        phone: "",
        assignableDuties: []
      },{
        id: 2,
        username: "",
        lastName: "Benoit",
        firstName: "Leon",
        email: "",
        phone: "",
        assignableDuties: []
      },{
        id: 3,
        username: "",
        lastName: "Benoit",
        firstName: "Alfonso",
        email: "",
        phone: "",
        assignableDuties: []
      },{
        id: 4,
        username: "",
        lastName: "Bailey",
        firstName: "Evan",
        email: "",
        phone: "",
        assignableDuties: []
      }]
    },
    {
      id: 1,
      name: "Work",
      duties: [{
        id: 0,
        name: "CR86 & 87",
        desc: "",
        assignedUsers: []
      }, {
        id: 1,
        name: "CR88 & 89",
        desc: "",
        assignedUsers: []
      }],
      users: [{
        id: 0,
        username: "",
        lastName: "Payne",
        firstName: "Jonathan",
        email: "",
        phone: "",
        assignableDuties: []
      },{
        id: 1,
        username: "",
        lastName: "Berry",
        firstName: "Javan",
        email: "",
        phone: "",
        assignableDuties: []
      },{
        id: 2,
        username: "",
        lastName: "Lorenz",
        firstName: "Nate",
        email: "",
        phone: "",
        assignableDuties: []
      },{
        id: 3,
        username: "",
        lastName: "Almodovar",
        firstName: "Paul",
        email: "",
        phone: "",
        assignableDuties: []
      },{
        id: 4,
        username: "",
        lastName: "Riley",
        firstName: "Bill",
        email: "",
        phone: "",
        assignableDuties: []
      }]
    }];
    */

    this.setActiveRoster = this.setActiveRoster.bind(this)
    this.addRoster = this.addRoster.bind(this)
    this.updateRoster = this.updateRoster.bind(this)
    this.removeRoster = this.removeRoster.bind(this)
    this.showDelRosterDialog = this.showDelRosterDialog.bind(this)
    this.hideDelRosterDialog = this.hideDelRosterDialog.bind(this)

    //Assign CRUD methods to objects
    let me = this;
    /*
    this.props.rosters.forEach((r) => {
      r.updateRoster = me.updateRoster
    })
    */

    let rosters = Rosters.find({owner: Meteor.user()._id}).fetch()
    let action = () => { this.setState({showDialog: false, dialogMsg: ""}) }

    this.state = {
      currentDelId: null,
      showDeleteDialog: false,
      showRosterAddedMsg: false,
      showDialog: false,
      dialogMsg: "",
      dialogHasYesNo: false,
      dialogYesAction: action,
      dialogNoAction: action,
      currentRoster: rosters[0],
      rosters: rosters
    };
  }

  setActiveRoster (rosterId) {
    let roster = this.state.rosters.filter( r => r._id === rosterId)[0];

    this.setState({currentRoster: roster});
  }

  /*
  addRoster (rosterName) {
    let rosters = this.state.rosters;

    //Sort and find highest ID
    let newestRoster = rosters.sort( (a, b) => b.id - a.id)[0];
    let id = newestRoster.id || -1;

    //Add new roster to array
    let roster = {
      id: id + 1,
      name: rosterName,
      duties: []
    }
    rosters.push(roster);

    //Update state and set as current roster
    this.setState({
      rosters: rosters,
      currentRoster: roster,
      showRosterAddedMsg: true
    });
  }
  */

  addRoster (rosterName) {
    //Add new roster to array
    Meteor.call("roster.addRoster", rosterName, (err, res) => {
      res
      .then(id => {
        this.setState({
          //rosters: Rosters.find().fetch(),
          currentRoster: Rosters.findOne(id)
        })
      })
    })
  }

  /*
  updateRoster (newRoster) {
    let rosters = this.state.rosters;

    let oldRoster = rosters.filter( r => r.id === newRoster.id )[0];

    let rosterIdx = rosters.indexOf(oldRoster);

    if (rosterIdx > -1) {
      rosters[rosterIdx] = newRoster;
      this.setState({rosters: rosters});
    }
  }
  */

  updateRoster (newRoster) {
    console.log(newRoster);
    //Rosters.update({_id: newRoster._id}, newRoster);
    console.log(Rosters.findOne(newRoster._id));
  }

  showDelRosterDialog (id) {
    this.setState({
      currentDelId: id,
      showDialog: true,
      dialogMsg: "Are you sure you want to delete this roster?",
      dialogHasYesNo: true,
      dialogYesAction: this.removeRoster,
      dialogNoAction: this.hideDelRosterDialog
    });
  }

  hideDelRosterDialog () {
    this.setState({
      currentDelId: null,
      showDialog: false,
      dialogMsg: ""
    });
  }

  showRosterAddedDialog () {
    let action = () => { this.setState({showDialog: false, dialogMsg: ""}) }

    this.setState({
      showDialog: true,
      dialogMsg: "New roster has been added!",
      dialogHasYesNo: false,
      dialogYesAction: action,
      dialogNoAction: action
    });
  }

  /*
  removeRoster () {
    let rosters = this.state.rosters;

    let roster = rosters.filter( r => r.id === this.state.currentDelId )[0];

    let rosterIdx = rosters.indexOf(roster);

    if (rosterIdx > -1) {
      rosters.splice(rosterIdx, 1);
      this.setState({rosters: rosters});
    }

    this.hideDelRosterDialog();
  }
  */

  removeRoster () {
    Rosters.remove(this.state.currentDelId, () => {
      let rosters = Rosters.find().fetch();

      this.setState({
        rosters: rosters,
        currentRoster: rosters[0]
      });
    });

    this.hideDelRosterDialog();
  }

  render () {
    return (
      <div id="myRostersView" className="w3-row">
        <div id="sideBar" className="w3-col s2 w3-container">
          <MyRostersList 	rosters={this.state.rosters}
            activeRoster={this.state.currentRoster}
            clickHandler={this.setActiveRoster}
            addRoster={this.addRoster}
            updateRoster={this.updateRoster}
            removeRoster={this.showDelRosterDialog}
          />
        </div>
        <MyRostersDetail roster={this.state.currentRoster}/>
        <DialogBox
          showDialog={this.state.showDialog}
          msg={this.state.dialogMsg}
          hasYesNo={this.state.dialogHasYesNo}
          yesAction={this.state.dialogYesAction}
          noAction={this.state.dialogNoAction}
        />
      </div>
    );
  }
}

export default MyRostersView;
