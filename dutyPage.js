import React from 'react'
import fontIcons from './fonts'
import DialogBox from './dialogBox'
import CheckBox from './checkBox'
import Duties from '../api/duties'
import NewDuty from './newDuty'
import { withTracker } from "meteor/react-meteor-data"
import './styles/dutyPage.scss'

class DutyPage extends React.Component {
  constructor (props) {
    super(props)


    //let duties = this.props.admin.duties

    let emptyDuty = {
      _id: -1,
      name: "",
      desc: "",
      scheduling: {
        scheduleType: "oneTime",
        scheduleDate: new Date(),
        occurance: "daily",
        schedInterval: 0,
        weekdayOpts: {
          sunday: false,
          monday: false,
          tuesday: false,
          wednesday: false,
          thursday: false,
          friday: false,
          saturday: false
        }
      }
    }

    this.state = {
      duties: [],
      emptyDuty: emptyDuty,
      currentDuty: emptyDuty,
      showDeleteDialog: false,
      showNewDutyDialog: false,
      dutiesUpdated: true
    }

    this.changeSelectedDuty = this.changeSelectedDuty.bind(this)
    this.changeFieldInput = this.changeFieldInput.bind(this)
    this.dutyNameOnBlur = this.dutyNameOnBlur.bind(this)
    this.showNewDutyDialog = this.showNewDutyDialog.bind(this)
    this.hideNewDutyDialog = this.hideNewDutyDialog.bind(this)
    this.removeDuty = this.removeDuty.bind(this)
    this.showDeleteDialog = this.showDeleteDialog.bind(this)
    this.hideDeleteDialog = this.hideDeleteDialog.bind(this)
    this.saveDuty = this.saveDuty.bind(this)
    this.renderUsers = this.renderUsers.bind(this)
    this.addRmvUser = this.addRmvUser.bind(this)
    this.selectSchedType =this.selectSchedType.bind(this)
    this.selectDays = this.selectDays.bind(this)
    this.isUserAssigned = this.isUserAssigned.bind(this)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.dutiesUpdated && !this.props.loading) {

      let duties = Duties.find({adminId: this.props.admin._id}).fetch()
      let currentDuty = (duties.length) ? duties[0] : this.state.emptyDuty

      currentDuty.inputDisplayName = currentDuty.name

      this.setState({
        duties: duties,
        currentDuty: currentDuty,
        dutiesUpdated: false
      })
    }

    /*
    if (this.props.admin.duties !== prevProps.admin.duties) {
      let duties = this.props.admin.duties;
      let currentDuty = duties[0];
      currentDuty.inputDisplayName = currentDuty.name;

      this.setState({
        duties: duties,
        currentDuty: currentDuty || this.state.emptyDuty
      });
    }
    */
  }

  showNewDutyDialog () {
    /*
    let newDuty = {_id: -1, rosterId: this.props.admin._id, sortId: 0, name: "New Duty", desc: "New Duty", inputDisplayName: "New Duty"};
    let duties = this.state.duties;

    let latestDuty = this.state.duties.sort((a,b) => b.sortId - a.sortId)[0] || newDuty;
    let newId = latestDuty.sortId + 1;

    newDuty.sortId = newId;

    duties.push(newDuty);

    this.setState({
      duties: duties,
      currentDuty: newDuty
    });
    */

    //Show New Duty Dialog
    this.setState({
      showNewDutyDialog: true
    })
  }

  hideNewDutyDialog () {
    //Show New Duty Dialog
    this.setState({
      showNewDutyDialog: false
    })
  }

  removeDuty () {
    let duty = this.state.currentDuty

    Duties.remove(duty._id, () => {
      let duties = Duties.find({rosterId: this.props.admin._id}).fetch()

      this.setState({
        duties: duties,
        currentDuty: duties[0] || this.state.emptyDuty,
        showDeleteDialog: false
      })
    })
  }

  renderDuties (duty, idx) {
    return (
      <option key={duty._id} id={"duty"+duty._id} value={duty._id}>{duty.name}</option>
    )
  }

  changeSelectedDuty (event) {
    if (event.target.value) {
      let currentDuty = this.state.duties.filter( d => d._id == event.target.value)[0]
      currentDuty.inputDisplayName = currentDuty.name

      this.setState({currentDuty: currentDuty})
    }
  }

  changeFieldInput (event) {
    let newVal = event.target.value;
    let duty = this.state.currentDuty;

    if (event.target.id === "dutyName") {
      duty.inputDisplayName = newVal;
    } else {
      duty.desc = newVal;
    }

    this.setState({currentDuty: duty})
  }

  dutyNameOnBlur () {
    let duty = this.state.currentDuty;
    duty.name = duty.inputDisplayName;

    this.setState({currentDuty: duty});
  }

  saveDuty () {

    let duty = this.state.currentDuty

    let setCurrentDuties = (function () {
      let duties = Duties.find({rosterId: this.props.admin._id}).fetch()

      this.setState({
        duties: duties,
        currentDuty: duties[0] || this.state.emptyDuty,
        showDeleteDialog: false
      })
    }).bind(this)

    if (duty._id === -1) {
      //Create duty
      delete duty._id
      Duties.insert(duty, setCurrentDuties)
    } else {
      //Modify duty
      Duties.update(duty._id, {$set: duty}, setCurrentDuties)
    }
  }

  showDeleteDialog () {
    this.setState({showDeleteDialog: true})
  }

  hideDeleteDialog () {
    this.setState({showDeleteDialog: false})
  }

  isUserAssigned (user) {
    let assignedUsers = this.state.currentDuty.assignedUsers

    return (assignedUsers) ? assignedUsers.indexOf(user.id) > -1 : false
  }

  addRmvUser (user) {
    let duty = this.state.currentDuty
    let assignedUsers = duty.assignedUsers
    let userIdx = assignedUsers.indexOf(user.id)

    if (userIdx > -1) {
      //Remove user from assigned
      assignedUsers.splice(userIdx, 1)
    } else {
      //Add user to assigned
      assignedUsers.push(user.id)
    }

    duty.assignedUsers = assignedUsers
    this.setState({currentDuty: duty})
  }

  selectSchedType (evt) {
    let currentDuty = this.state.currentDuty
    currentDuty.scheduling.scheduleType = evt.currentTarget.value

    this.setState({currentDuty: currentDuty})
  }

  selectDays (day) {
    let currentDuty = this.state.currentDuty
    let currentDayVal = currentDuty.scheduling.weekdays[day]

    currentDuty.scheduling.weekdays[day] = !currentDayVal
    this.setState({currentDuty: currentDuty})
  }

  renderUsers (user) {
    if (user && this.state.currentDuty._id !== -1) {
      let assigned = this.isUserAssigned(user)

      return (
        <li key={user.id} className="w3-row">
          <div className="w3-threequarter">
            <span className="">{user.firstName + " " + user.lastName}</span>
          </div>
          <div className="w3-rest">
            <CheckBox
              isChecked={assigned}
              onClick={() => this.addRmvUser(user)}
            />
          </div>
        </li>
      )
    }
  }

  render () {
    return (
      <div className="duties-pg">
        <div className="w3-row duties-hdr">
          <div className="w3-col w3-third">
            <span className="w3-left-align duties-pg-hdr">Current Duties</span>
            <button className="new-duty-btn" onClick={this.showNewDutyDialog}>{fontIcons.addIcon}New Duty</button>
          </div>
          <div className="w3-col w3-third">
            <span className="w3-left-align duties-pg-hdr">Available Users</span>
          </div>
          <div className="w3-col w3-third">
            <span className="w3-left-align duties-pg-hdr">Scheduling</span>
          </div>
        </div>
        <div className="w3-row duties-list">
          <div className="w3-col w3-third">
            <select
              className="w3-left-align"
              multiple
              onChange={this.changeSelectedDuty}
              value={[this.state.currentDuty._id]}
            >
              {this.state.duties.map(this.renderDuties)}
            </select>
          </div>
          <div className="w3-col w3-third">
            <ul className="user-list">
              {(this.props.admin.users) ? this.props.admin.users.map(this.renderUsers) : []}
            </ul>
          </div>
          <div className="w3-col w3-third">
            {/*
            <label id="dutyNameLbl">Duty Name</label>
            <input
              type="text"
              id="dutyName"
              value={this.state.currentDuty.inputDisplayName || ""}
              onChange={this.changeFieldInput}
              disabled={this.state.duties.length === 0}
              onBlur={this.dutyNameOnBlur}
            />
            <label id="dutyDescLbl">Duty Description</label>
            <textarea
              id="dutyDesc"
              value={this.state.currentDuty.desc || ""}
              onChange={this.changeFieldInput}
              disabled={this.state.duties.length === 0}
            />
            <div className="w3-bar control-bar">
              <button
                className="w3-btn w3-ripple w3-border w3-border-teal w3-round-large"
                onClick={this.saveDuty}
              >Save Duty</button>
              <button
                className="w3-btn w3-ripple w3-border w3-border-pink w3-round-large"
                onClick={this.showDeleteDialog}
              >Remove Duty</button>
            </div>
            */}
            <div className="schedule-type-opts">
              <p className="heading">Schedule Type</p>
              <input  type="radio"
                      name="scheduleType"
                      value="oneTime"
                      checked={this.state.currentDuty.scheduling.scheduleType === "oneTime"}
                      onChange={this.selectSchedType}
                      disabled />
              <span>One-time</span>
              <input  type="radio"
                      name="scheduleType"
                      value="recurring"
                      checked={this.state.currentDuty.scheduling.scheduleType === "recurring"}
                      onChange={this.selectSchedType}
                      disabled />
              <span>Recurring</span>
            </div>
            <div className="one-time-sched-opts">
              <p className="heading">Choose Date</p>
              <input type="date" name="scheduleDate" />
            </div>
            <div className="recurring-sched-opts">
              <p className="heading">How Often</p>
              <select className="occurance-select">
                <option value="daily">Daily</option>
                <option value="weekly">Weekly</option>
                <option value="monthly">Monthly</option>
              </select>
              <div className="sched-interval-cntnr">
                <span>Every</span><input type="text" className="sched-interval" /><span>Days</span>
              </div>
              <div className="weekday-opts-cntnr">
                <div  className={"weekday-opts" + ((this.state.currentDuty.scheduling.weekdayOpts.sunday) ? " active" : "")}
                      onClick={() => this.selectDays("sunday")}>S</div>
                <div  className={"weekday-opts" + ((this.state.currentDuty.scheduling.weekdayOpts.monday) ? " active" : "")}
                      onClick={() => this.selectDays("monday")}>M</div>
                <div  className={"weekday-opts" + ((this.state.currentDuty.scheduling.weekdayOpts.tuesday) ? " active" : "")}
                      onClick={() => this.selectDays("tuesday")}>Tu</div>
                <div  className={"weekday-opts" + ((this.state.currentDuty.scheduling.weekdayOpts.wednesday) ? " active" : "")}
                      onClick={() => this.selectDays("wednesday")}>W</div>
                <div  className={"weekday-opts" + ((this.state.currentDuty.scheduling.weekdayOpts.thursday) ? " active" : "")}
                      onClick={() => this.selectDays("thursday")}>Th</div>
                <div  className={"weekday-opts" + ((this.state.currentDuty.scheduling.weekdayOpts.friday) ? " active" : "")}
                      onClick={() => this.selectDays("friday")}>F</div>
                <div  className={"weekday-opts" + ((this.state.currentDuty.scheduling.weekdayOpts.saturday) ? " active" : "")}
                      onClick={() => this.selectDays("saturday")}>S</div>
              </div>
            </div>
          </div>
        </div>
        <DialogBox
          msg="Are you sure you want to delete this duty?"
          hasYesNo={true}
          showDialog={this.state.showDeleteDialog}
          yesAction={this.removeDuty}
          noAction={this.hideDeleteDialog}
        />
        <NewDuty
          showDialog={this.state.showNewDutyDialog}
          close={this.hideNewDutyDialog}
          admin={this.props.admin}
          dutyPage={this}
        />
      </div>
    )
  }
}

export default withTracker((props) => {
  const dutiesHandle = Meteor.subscribe('duties', props.admin._id)
  const loading = !dutiesHandle.ready()

  return {
    loading: loading,
    admin: props.admin
  };
})(DutyPage)
