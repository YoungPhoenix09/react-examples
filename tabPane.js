import React from 'react';
import './styles/tabPane.css';

class TabPane extends React.Component {
  constructor (props) {
    super(props);

    this.state = {currentTab: this.props.tabs[0]};

    this.renderTabs = this.renderTabs.bind(this);
    this.switchTab = this.switchTab.bind(this);
  }

  componentDidUpdate (prevProps) {
    if (prevProps.tabs != this.props.tabs) {
      this.setState({currentTab: this.props.tabs[0]});
    }
  }

  renderTabs(tab, idx) {
    let activeTab = (this.state.currentTab === tab) ? " active" : "";

    return (
      <button key={tab.name} className={"w3-bar-item w3-button w3-border" + activeTab}
        onClick={ () => this.switchTab(tab) }
      >{tab.name}</button>
    )
  }

  switchTab (tab) {
    this.setState({currentTab: tab});
  }

  render () {
    return (
      <div className={this.props.className + " tabPane w3-container"}>
        <div className="w3-bar tabPaneNav">
          {this.props.tabs.map(this.renderTabs)}
        </div>
        <div className="w3-container tabPaneView  w3-border">
          {this.state.currentTab.view}
        </div>
      </div>
    )
  }
}

export default TabPane;
