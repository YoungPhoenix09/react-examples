import React from 'react'
import { Accounts } from "meteor/accounts-base"
import InputMask from 'react-input-mask'
import './styles/signUp.scss'

class SignUp extends React.Component {
  constructor (props) {
    super(props)
    //Check if user is logged in

    //Set state based on value
    this.state = {
      showSignUp: false,
      showNext: false,
      showComplete: false,
      completionTitle: "",
      completionMsg: "",
      canContinue: false,
      canRegister: false,
      signUpUser: "",
      signUpEmail: "",
      signUpPwrd: "",
      signUpCnfrmPwrd: "",
      signUpFirstName: "",
      signUpLastName: "",
      signUpPhone: "",
      isUsrValid: true,
      isPwdValid: true,
      isCnfPwdValid: true,
      isEmailValid: true,
      isFstNameValid: true,
      isLstNameValid: true,
      isPhnValid: true,
      usrValidationMsg: "",
      pwdValidationMsg: "",
      cnfPwdValidationMsg: "",
      emailValidationMsg: "",
      fstNameValidationMsg: "",
      lstNameValidationMsg: "",
      phnValidationMsg: ""
    }

    //Method bindings
    this.signUp = this.signUp.bind(this)
    this.closeSignUp = this.closeSignUp.bind(this)
    this.continueSignUp = this.continueSignUp.bind(this)
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.validateFields = this.validateFields.bind(this)
    this.showValidationMsgs = this.showValidationMsgs.bind(this)
  }

  closeSignUp () {
    this.setState({
      showSignUp: false,
      showNext: false,
      showComplete: false,
      completionTitle: "",
      completionMsg: "",
      canContinue: false,
      canRegister: false,
      signUpUser: "",
      signUpEmail: "",
      signUpPwrd: "",
      signUpCnfrmPwrd: "",
      signUpFirstName: "",
      signUpLastName: "",
      signUpPhone: "",
      isUsrValid: true,
      isPwdValid: true,
      isCnfPwdValid: true,
      isEmailValid: true,
      isFstNameValid: true,
      isLstNameValid: true,
      isPhnValid: true,
      usrValidationMsg: "",
      pwdValidationMsg: "",
      cnfPwdValidationMsg: "",
      emailValidationMsg: "",
      fstNameValidationMsg: "",
      lstNameValidationMsg: "",
      phnValidationMsg: ""
    })
  }

  handleFieldChange (e) {
    let target = e.currentTarget.id

    let runValidation = () => {
      let inputTrgt = target
      this.validateFields(inputTrgt)
    }

    switch (e.currentTarget.id) {
      case "signUpUser":
        this.setState({
          signUpUser: e.currentTarget.value
        }, runValidation)
        break
      case "signUpEmail":
        this.setState({
          signUpEmail: e.currentTarget.value
        }, runValidation)
        break
      case "signUpPwrd":
        this.setState({
          signUpPwrd: e.currentTarget.value
        }, runValidation)
        break
      case "signUpCnfrmPwrd":
        this.setState({
          signUpCnfrmPwrd: e.currentTarget.value
        }, runValidation)
        break
      case "signUpFirstName":
        this.setState({
          signUpFirstName: e.currentTarget.value
        }, runValidation)
        break
      case "signUpLastName":
        this.setState({
          signUpLastName: e.currentTarget.value
        }, runValidation)
        break
      case "signUpPhone":
        this.setState({
          signUpPhone: e.currentTarget.value
        }, runValidation)
        break
    }
  }

  async validateFields (target) {
    let valid
    let msg

    //User can continue if no validation msgs present
    let activateBtns = () => {
      this.setState({
        canContinue: (this.state.signUpUser && !this.state.usrValidationMsg) &&
          (this.state.signUpEmail && !this.state.emailValidationMsg) &&
          (this.state.signUpPwrd && !this.state.pwdValidationMsg) &&
          (this.state.signUpCnfrmPwrd && !this.state.cnfPwdValidationMsg),
        canRegister: (this.state.signUpFirstName && !this.state.fstNameValidationMsg) &&
          (this.state.signUpLastName && !this.state.lstNameValidationMsg) &&
          (this.state.signUpPhone && !this.state.phnValidationMsg)
      })
    }

    //Promise for meteor method to validate username
    if (!this.state.showNext) {
      let userValidate = new Promise((resolve, reject) => {
        Meteor.call("signUp.validateField", "user", this.state.signUpUser, (err, result) => {
          if (err) {
            console.log(err)
            reject("Uh oh, something's wrong!  Please try again later.")
          } else {
            resolve(result)
          }
        })
      })

      //Promise for meteor method to validate email
      let emailValidate = new Promise((resolve, reject) => {
        Meteor.call("signUp.validateField", "email", this.state.signUpEmail, (err, result) => {
          if (err) {
            console.log(err)
            reject("Uh oh, something's wrong!  Please try again later.")
          } else {
            resolve(result)
          }
        })
      })

      switch (target) {
        case "signUpUser":
          //Validate username
          msg = await userValidate
          this.setState({
            usrValidationMsg: msg,
            isUsrValid: true
          }, activateBtns)
          break
        case "signUpEmail":
          //Validate email
          msg = await emailValidate
          this.setState({
            emailValidationMsg: msg,
            isEmailValid: true
          }, activateBtns)
          break
        case "signUpPwrd":
        case "signUpCnfrmPwrd":
          //Validate password
          valid = (!!this.state.signUpPwrd.match(/\d+/)
            && !!this.state.signUpPwrd.match(/[A-Z]+/)
            && !!this.state.signUpPwrd.match(/[a-z]+/)
            && !!this.state.signUpPwrd.match(/^\S{8,16}$/)
          )
          this.setState({
            pwdValidationMsg: (valid) ? "" : "Password must have 1 upper and lower case letter, 1 digit and a length of 8-16 characters",
            isPwdValid: true
          }, activateBtns)

          //Validate confirmation password
          valid = this.state.signUpPwrd === this.state.signUpCnfrmPwrd
          this.setState({
            cnfPwdValidationMsg: (valid) ? "" : "Passwords do not match",
            isCnfPwdValid: true
          }, activateBtns)
          break
      }
    } else {

      switch (target) {
        case "signUpFirstName":
          //Validate first name
          valid = !!this.state.signUpFirstName.match(/^[A-Za-z]{1,30}$/)
          this.setState({
            fstNameValidationMsg: (valid) ? "" : "Must be a 1-30 character alphabetic name",
            isFstNameValid: true
          }, activateBtns)
          break
        case "signUpLastName":
          //Validate last name
          valid = !!this.state.signUpLastName.match(/^[A-Za-z]{1,30}$/)
          this.setState({
            lstNameValidationMsg: (valid) ? "" : "Must be a 1-30 character alphabetic name",
            isLstNameValid: true
          }, activateBtns)
          break
        case "signUpPhone":
          //Validate phone
          valid = !!this.state.signUpPhone.match(/^\(\d\d\d\) \d\d\d-\d\d\d\d$/)
          this.setState({
            phnValidationMsg: (valid) ? "" : "Phone number is not valid",
            isPhnValid: true
          }, activateBtns)
          break
      }
    }
  }

  showValidationMsgs (e) {
    switch (e.currentTarget.id) {
      case "signUpUser":
        this.setState({
          isUsrValid: !this.state.usrValidationMsg
        })
        break
      case "signUpEmail":
        this.setState({
          isEmailValid: !this.state.emailValidationMsg
        })
        break
      case "signUpPwrd":
      case "signUpCnfrmPwrd":
        this.setState({
          isPwdValid: !this.state.pwdValidationMsg,
          isCnfPwdValid: !this.state.cnfPwdValidationMsg
        })
        break
      case "signUpFirstName":
        this.setState({
          isFstNameValid: !this.state.fstNameValidationMsg
        })
        break
      case "signUpLastName":
        this.setState({
          isLstNameValid: !this.state.lstNameValidationMsg
        })
        break
      case "signUpPhone":
        this.setState({
          isPhnValid: !this.state.phnValidationMsg
        })
        break
    }
  }

  continueSignUp (e) {
    //Stop refresh
    e.preventDefault()

    this.setState({showNext: true})
  }

  signUp (e) {
    //Stop refresh
    e.preventDefault()

    let profile = {
      lname: this.state.signUpLastName,
      fname: this.state.signUpFirstName,
      phone: this.state.signUpPhone
    }

    Accounts.createUser({
      username: this.state.signUpUser,
      email: this.state.signUpEmail,
      password: this.state.signUpPwrd,
      profile: profile
    }, (err) => {
      if (err) {
        console.log(err)
        this.setState({
          showComplete: true,
          completionTitle: "Uh oh!",
          completionMsg: "Something went wrong.  Please try again later.",
        })
      } else {
        let completionMsg = "An email has been sent for you to validate your account."
        console.log("successfully created user!")

        let user = Meteor.users.findOne({username: this.state.signUpUser})

        //Send email
        if (user) {
          Meteor.call('email.sendEmailVerification', user._id)
        } else {
          completionMsg = "You'll need to verify your email once you log in."
        }

        this.setState({
          showComplete: true,
          completionTitle: "That's It!",
          completionMsg: completionMsg,
        }, () => {
          //Handle any invites the user may have and add the admins to the user
          Meteor.call("usrInviteRequests.handleUsrInviteRequest", this.state.signUpEmail)
        })
      }
    })
  }

  render () {
    let fields = (
      <div className="acct-fields">
        <div className="w3-row">
          <label htmlFor="signUpUser">User Name</label>
          <input  type="text"
                  id="signUpUser"
                  className={"w3-input w3-border w3-round" + (this.state.isUsrValid ? "" : " invalid")}
                  onChange={this.handleFieldChange}
                  onBlur={this.showValidationMsgs}
                  value={this.state.signUpUser} />
          <div className={(!this.state.isUsrValid) ? "validation-msg" : "w3-hide"}>
            {this.state.usrValidationMsg}
          </div>
        </div>
        <div className="w3-row">
          <label htmlFor="signUpPwrd">Password</label>
          <input  type="password"
                  id="signUpPwrd"
                  className={"w3-input w3-border w3-round" + (this.state.isPwdValid ? "" : " invalid")}
                  onChange={this.handleFieldChange}
                  onBlur={this.showValidationMsgs}
                  value={this.state.signUpPwrd} />
          <div className={(!this.state.isPwdValid) ? "validation-msg" : "w3-hide"}>
            {this.state.pwdValidationMsg}
          </div>
        </div>
        <div className="w3-row">
          <label htmlFor="signUpCnfrmPwrd">Confirm Password</label>
          <input  type="password"
                  id="signUpCnfrmPwrd"
                  className={"w3-input w3-border w3-round" + (this.state.isCnfPwdValid ? "" : " invalid")}
                  onChange={this.handleFieldChange}
                  onBlur={this.showValidationMsgs}
                  value={this.state.signUpCnfrmPwrd} />
          <div className={(!this.state.isCnfPwdValid) ? "validation-msg" : "w3-hide"}>
            {this.state.cnfPwdValidationMsg}
          </div>
        </div>
        <div className="w3-row">
          <label htmlFor="signUpEmail">Email</label>
          <input  type="email"
                  id="signUpEmail"
                  className={"w3-input w3-border w3-round" + (this.state.isEmailValid ? "" : " invalid")}
                  onChange={this.handleFieldChange}
                  onBlur={this.showValidationMsgs}
                  value={this.state.signUpEmail} />
          <div className={(!this.state.isEmailValid) ? "validation-msg" : "w3-hide"}>
            {this.state.emailValidationMsg}
          </div>
        </div>
      </div>
    )

    if (this.state.showNext) {
      fields = (
        <div className="acct-fields">
          <div className="w3-row">
            <label htmlFor="signUpFirstName">First Name</label>
            <input  type="text"
                    id="signUpFirstName"
                    className={"w3-input w3-border w3-round" + (this.state.isFstNameValid ? "" : " invalid")}
                    onChange={this.handleFieldChange}
                    onBlur={this.showValidationMsgs}
                    value={this.state.signUpFirstName} />
            <div className={(!this.state.isFstNameValid) ? "validation-msg" : "w3-hide"}>
              {this.state.fstNameValidationMsg}
            </div>
          </div>
          <div className="w3-row">
            <label htmlFor="signUpLastName">Last Name</label>
            <input  type="text"
                    id="signUpLastName"
                    className={"w3-input w3-border w3-round" + (this.state.isLstNameValid ? "" : " invalid")}
                    onChange={this.handleFieldChange}
                    onBlur={this.showValidationMsgs}
                    value={this.state.signUpLastName} />
            <div className={(!this.state.isLstNameValid) ? "validation-msg" : "w3-hide"}>
              {this.state.lstNameValidationMsg}
            </div>
          </div>
          <div className="w3-row">
            <label htmlFor="signUpPhone">Phone</label>
            <InputMask  type="text"
                    id="signUpPhone"
                    className={"w3-input w3-border w3-round" + (this.state.isPhnValid ? "" : " invalid")}
                    mask="(999) 999-9999"
                    onChange={this.handleFieldChange}
                    onBlur={this.showValidationMsgs}
                    value={this.state.signUpPhone} />
            <div className={(!this.state.isPhnValid) ? "validation-msg" : "w3-hide"}>
              {this.state.phnValidationMsg}
            </div>
          </div>
        </div>
      )
    }

    return (
      <div ref={this.props.innerRef} className="sign-up w3-container">
        {(this.state.showComplete) ? (
          <>
            <h2>{this.state.completionTitle}</h2>
            <p>{this.state.completionMsg}</p>
            <button onClick={() => {
              //Changes login status to show user's page
              this.props.app.setState({
                isSignedIn: !!Meteor.userId()
              });

              this.closeSignUp
            }}
                    className="submit">
              Okay
            </button>
          </>
        ) : (
          <>
            <h2>{(this.state.showNext) ? "Tell us a little about yourself" : "Welcome To RosterMe"}</h2>
            <form onSubmit={(this.state.showNext) ? this.signUp : this.continueSignUp}>
              {fields}
              {/*<button onClick={(this.state.showNext) ? this.signUp : this.continueSignUp}
                      disabled={!this.state.showNext && !this.state.canContinue || this.state.showNext && !this.state.canRegister}
                      className="submit" >
                {(this.state.showNext) ? "Register" : "Continue"}
              </button>*/}
              <input  disabled={!this.state.showNext && !this.state.canContinue || this.state.showNext && !this.state.canRegister}
                      className="submit"
                      type="submit"
                      value={(this.state.showNext) ? "Register" : "Continue"}
              />
            </form>
          </>
        )}
      </div>
    )
  }
}

export default React.forwardRef((props, ref) => <SignUp innerRef={ref} app={props.app} />);
