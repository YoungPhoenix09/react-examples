import React from 'react';
import './styles/loadingSpinner.scss';

class LoadingSpinner extends React.Component {
  render () {
    const bgColor = "#00000000"

    return (
      <div className="loading-container w3-display-container">
        <div className="w3-display-middle">
          <p className="loading-msg">Please Wait...</p>
          <svg height="100" width="100" className="spinner">
            <defs>
              <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                <stop offset="0%" style={{stopColor:"rgb(0, 102, 204)", stopOpacity: "1"}} />
                <stop offset="100%" style={{stopColor:"rgb(0, 204, 255)", stopOpacity: "1"}} />
              </linearGradient>
            </defs>
            <circle  cx="50" cy="50" r="32" stroke="url(#grad1)" strokeWidth="18" fill="transparent"/>
          </svg>
        </div>
      </div>
    )
  }
}

export default LoadingSpinner
