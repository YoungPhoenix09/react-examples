import React from 'react'
import { Meteor } from 'meteor/meteor'
import { SelectionState, DataTypeProvider, SearchState, IntegratedFiltering } from '@devexpress/dx-react-grid'
import { Grid, Table, TableHeaderRow, TableSelection, SearchPanel, Toolbar } from '@devexpress/dx-react-grid-material-ui'
import RowLoadingSpinner from './rowLoadingSpinner'
import Admins from '../api/administrations'
import LoadingSpinner from './loadingSpinner'
import { withTracker } from "meteor/react-meteor-data"
import "./styles/adminsSearch.scss"

class AdminsSearch extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      admins: []
    }

    this.getAllAdmins = this.getAllAdmins.bind(this)
    this.joinAdmin = this.joinAdmin.bind(this)

    this.getAllAdmins()
  }

  async getAllAdmins () {
    let admins = await new Promise((resolve, reject) => {
      Meteor.call("admin.getAllAdmins", (err, admins) => {
        if (err) {
          resolve([])
        } else {
          resolve(admins)
        }
      })
    })

    this.setState({admins: admins})
  }

  joinAdmin (e) {
    let component = this
    let adminId = e.currentTarget.parentNode.dataset.adminId
    let admins = component.state.admins
    let admin = admins.filter((admin) => admin._id === adminId)[0]
    let adminIdx = admins.indexOf(admin)

    new Promise((resolve, reject) => {
      //Show working
      admins[adminIdx].isWorking = true
      component.setState({admins: admins}, () => {
        resolve()
      })
    })
    .then(() => {
      let callPromise = new Promise((resolve, reject) => {
        //Update admin/user relationship
        Meteor.call("admin.setAdminUserRelationship", adminId, Meteor.userId(), "request", () => {
          resolve()
        })
      })

      return callPromise
    })
    .then(() => {
      //Refresh list
      this.getAllAdmins()

      //Stop showing working
      admins[adminIdx].isWorking = false
      component.setState({admins: admins})
    })
  }

  render () {
    let cols = [
      {name: "name", title: "Administration"},
      {name: "owner", title: "Owner"},
      {name: "users", title: "Users"},
      {name: "_id", title: " "},
    ]

    let predicateFn = (value, filter) => value.toLowerCase().indexOf(filter.value.toLowerCase()) > -1
    let disableFn = (value, filter) => false
    let colPredicates = [
      {
        columnName: "name",
        predicate: predicateFn
      },
      {
        columnName: "owner",
        predicate: predicateFn
      },
      {
        columnName: "users",
        predicate: disableFn
      },
      {
        columnName: "_id",
        predicate: disableFn
      },
    ]

    return (
      <div className="admin-search">
        <div className="admin-search-hdr">
          <h1>Administrations Search</h1>
        </div>
        <div className="admins-grid">
          <Grid rows={this.state.admins} columns={cols}>
            <SearchState />
            <IntegratedFiltering columnExtensions={colPredicates}/>
            <Table />
            <DataTypeProvider for={["_id"]} formatterComponent={({ value, row }) => {
              let isMember = false
              let isOwner = false

              //Check if user is owner
              if (row.ownerId === Meteor.userId()) {
                isMember = true
                isOwner = true
              } else {
                //Check if user is pending for the administration
                let user = Meteor.user()
                let admin = Admins.find({
                  _id: row._id,
                  users: {$elemMatch: {userEmail: (user) ? user.emails[0].address : ""}}
                }).fetch()[0]

                isMember = !!admin
              }

              let elem
              if (isMember) {
                elem = (<div>{(isOwner) ? "Owned" : "Joined"}</div>)
              } else {
                elem = (
                  <div className="admin-controls" data-admin-id={value}>
                    <span title="Join Administration" onClick={this.joinAdmin}>
                      <button>Join</button>
                    </span>
                    <RowLoadingSpinner isShowing={!!row.isWorking}/>
                  </div>
                )
              }

              return elem
            }} />
            <TableHeaderRow />
            <Toolbar />
            <SearchPanel  className="admin-search-input"
                          messages={{searchPlaceholder: "Search administration or owner"}}/>
          </Grid>
        </div>
        {(this.props.loading) ? (<LoadingSpinner />) : ""}
      </div>
    )
  }
}

//export default AdminsSearch
export default withTracker(() => {
  let loading

  if (Meteor.user()) {
    let owner = Meteor.user()._id
    const adminHandle = Meteor.subscribe('admins', owner)
    loading = !adminHandle.ready()
  } else {
    loading = true
  }

  return {
    loading: loading
  }
})(AdminsSearch)
