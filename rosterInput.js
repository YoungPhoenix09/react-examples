import React from "react";
import ReactDOM from "react-dom";

class RosterInput extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      hasErr: false
    }

    this.validateRoster = this.validateRoster.bind(this);
    this.lostFocus = this.lostFocus.bind(this);
  }

  componentDidUpdate () {
    if (this.props.showing) {
      this.inputField.focus();
    }
  }

  validateRoster (e) {
    //Stops refresh/redirect of page due
    if (e) {
      e.preventDefault();
    }

    //Check values
    let inputVal = this.inputField.value;

    if (inputVal.match(/^[A-Za-z](\s|[A-Za-z0-9]){0,29}$/)) {
      //Save roster
      if (this.props.id !== undefined) {
        this.props.save(this.props.id, inputVal);
      } else {
        this.props.save(inputVal);
      }
    } else {
      this.setState({hasErr: true});
      this.inputField.value = "";
    }
  }

  lostFocus () {
    if (this.props.cancel) {
      this.props.cancel();
    }
  }

  render () {
    if (this.props.showing) {
      return (
        <form onSubmit={this.validateRoster}>
          <input  placeholder={(this.state.hasErr) ? "Invalid Name" : "Roster Name"}
            type="text"
            ref={ input => this.inputField = input}
            onBlur={this.lostFocus}
            className={(this.state.hasErr) ? "w3-border-red" : ""}
            onClick={ e => e.stopPropagation() }
          />
          <input type="submit" className= "w3-hide" />
        </form>
      )
    } else {
      return (<div></div>)
    }
  }
}

export default RosterInput;
