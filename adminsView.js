import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'
import { SelectionState, DataTypeProvider } from '@devexpress/dx-react-grid'
import { Grid, Table, TableHeaderRow, TableSelection } from '@devexpress/dx-react-grid-material-ui'
import UsersPage from './usersPage'
import DutyPage from './dutyPage'
import Admins from '../api/administrations'
import RowLoadingSpinner from './rowLoadingSpinner'
import FontIcons from './fonts'
import LoadingSpinner from './loadingSpinner'
import { withTracker } from "meteor/react-meteor-data"
import './styles/adminsView.scss'

class AdminsView extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isNewInputShowing: false,
      inputAdminName: "",
      isAdminNameValid: false,
      admins: [],
      isDataInitialized: false,
      isAdminViewHidden: true,
      currentAdminView: "user"
    }

    this.getAdminList = this.getAdminList.bind(this)
    this.updateInputAdminName = this.updateInputAdminName.bind(this)
    this.validateAdminName = this.validateAdminName.bind(this)
    this.addNewAdmin = this.addNewAdmin.bind(this)
    this.acceptAdminInvite = this.acceptAdminInvite.bind(this)
    this.showNewAdminInput = this.showNewAdminInput.bind(this)
    this.cancelNewAdmin = this.cancelNewAdmin.bind(this)
    this.deleteAdmin = this.deleteAdmin.bind(this)
    this.showAdminView = this.showAdminView.bind(this)
    this.hideAdminView = this.hideAdminView.bind(this)
    this.changeAdminView = this.changeAdminView.bind(this)
  }

  componentDidMount () {
    this.getAdminList()
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.viewType !== this.props.viewType) {
      this.getAdminList()
      this.hideAdminView()
    }
  }

  async getAdminList () {
    let admins = await this.adminListWrapper()

    this.setState({
      admins: admins
    })
  }

  adminListWrapper () {
    let callname = ""
    let arg = null

    if (this.props.viewType === "joined") {
      let user = Meteor.user()

      if (!user) {
        //Send empty array if no user found
        resolve([])
      }

      callname = "admin.getJoinedAdmins"
      arg = user.emails[0].address
    } else {
      callname = "admin.getMyAdmins"
      arg = Meteor.userId()
    }

    let adminPromise = new Promise(function (resolve, reject) {
      let getAdminsCallback = (err, admins) => {
        if (err) {
          reject(err)
        } else {
          resolve(admins)
        }
      }

      Meteor.call(callname, arg, getAdminsCallback)
    })

    return adminPromise
  }

  showNewAdminInput () {
    this.setState({
      isNewInputShowing: true
    })
  }

  cancelNewAdmin () {
    this.setState({
      isNewInputShowing: false,
      inputAdminName: "",
      isAdminNameValid: false
    })
  }

  updateInputAdminName (e) {
    this.setState({
      inputAdminName: e.currentTarget.value
    }, this.validateAdminName)
  }

  validateAdminName () {
    //Check values
    let inputVal = this.state.inputAdminName;

    this.setState({isAdminNameValid: inputVal.match(/^[A-Za-z](\s|[A-Za-z0-9]){0,29}$/)})
  }

  addNewAdmin () {
    //Add new Admin to array
    Meteor.call("admin.addAdmin", this.state.inputAdminName, async (err, id) => {
      let admins = await this.adminListWrapper()

      this.setState({
        //admins: Admins.find().fetch(),
        admins: admins,
        currentAdmin: Admins.findOne(id),
        inputAdminName: "",
        isNewInputShowing: false,
        isAdminViewHidden: false
      })
    })
  }

  acceptAdminInvite (e) {
    let component = this
    let id = e.currentTarget.parentNode.dataset.adminId
    let userEmail = Meteor.user().emails[0].address
    let admins = component.state.admins
    let admin = admins.filter((admin) => admin._id === id)[0]
    let adminIdx = admins.indexOf(admin)

    //Set working status of admin
    admins[adminIdx].isWorking = true
    component.setState({admins: admins}, () => {
      Meteor.call("admin.approvePending", id, userEmail, (err) => {
        if (!err) {
          component.getAdminList()
        }

        admins = component.state.admins
        admins[adminIdx].isWorking = false
        component.setState({admins: admins})
      })
    })
  }

  deleteAdmin (e) {
    let id = e.currentTarget.parentNode.dataset.adminId

    Meteor.call("admin.deleteAdmin", id, (err) => {
      if (!err) {
        this.getAdminList()
      }
    })
  }

  showAdminView (e) {
    let id = e.currentTarget.parentNode.dataset.adminId
    let currentAdmin = Admins.findOne({_id: id})

    this.setState({
      currentAdmin: currentAdmin,
      isAdminViewHidden: false
    })
  }

  hideAdminView () {
    this.setState({
      currentAdmin: null,
      isAdminViewHidden: true
    })
  }

  changeAdminView (e) {
    console.log(e.currentTarget);

    switch (e.currentTarget.id) {
      case "adminShowUsers":
        this.setState({currentAdminView: "user"})
        break
      case "adminShowDuties":
        this.setState({currentAdminView: "duty"})
        break
      case "adminShowSched":
        this.setState({currentAdminView: "schedule"})
    }
  }

  render () {
    let userName = (Meteor.user()) ? Meteor.user().username : ""
    let columns = [
      { name: 'name', title: 'Administration' },
      { name: 'owner', title: 'Owner' },
      { name: '_id', title: ' '}
    ]
    let tableNoDataCellComponent = (props) => (
      <Table.NoDataCell className="no-admin-data" colSpan={columns.length} getMessage={(messageKey) => "NOTHING HERE FOR NOW"}/>
    )
    let adminView = ""

    switch (this.state.currentAdminView) {
      case "user":
        adminView = <UsersPage admin={this.state.currentAdmin}/>
        break
      case "duty":
        adminView = <DutyPage admin={this.state.currentAdmin}/>
        break
      case "schedule":
        adminView = <div></div>
    }

    return (
      <div className="main-view-container">
        <button onClick={this.hideAdminView}
                className={this.state.isAdminViewHidden ? "w3-hide" : "back-btn"}>Back</button>

        {(this.state.isAdminViewHidden) ? (
          <div className="admin-list-view">
            <div className="admin-list-hdr">
              <h1>{"Welcome, " + userName}</h1>
              <h3>{((this.props.viewType === "owned") ? "My" : "Joined") + " Administrations"}</h3>
              <button onClick={this.showNewAdminInput}
                      className={this.state.isNewInputShowing || !this.state.isAdminViewHidden || this.props.viewType === "joined" ? "w3-hide" : ""}>New Admin</button>
              <span className={this.state.isNewInputShowing ? "new-admin" : "w3-hide"}>
                <input  className="w3-input admin-input" type="text" value={this.state.inputAdminName}
                        onChange={this.updateInputAdminName}
                        placeholder="Admin Name"/>
                <button className={this.state.isNewInputShowing ? "w3-button" : "w3-hide"}
                        disabled={!this.state.isAdminNameValid}
                        onClick={this.addNewAdmin}>Create</button>
                <button className={this.state.isNewInputShowing ? "w3-button" : "w3-hide"}
                        onClick={this.cancelNewAdmin}>Cancel</button>
              </span>
            </div>
            <div className="admin-list-grid">
              <Grid
                rows={this.state.admins}
                columns={columns}>
                <Table noDataCellComponent={tableNoDataCellComponent}/>
                <DataTypeProvider for={["_id"]} formatterComponent={({ value, row }) => {
                  let invitePending = false
                  let requestPending = false

                  //Check if user is owner
                  if (row.ownerId === Meteor.userId()) {
                    invitePending = false
                    requestPending = false
                  } else {
                    //Check if user is pending for the administration
                    if (Meteor.user()) {
                      invitePending = !!Admins.findOne({
                        _id: row._id,
                        users: {$elemMatch: {userEmail: Meteor.user().emails[0].address, isPending: true, joinMethod: "invite"}}
                      })

                      requestPending = !!Admins.findOne({
                        _id: row._id,
                        users: {$elemMatch: {userEmail: Meteor.user().emails[0].address, isPending: true, joinMethod: "request"}}
                      })
                    }
                  }

                  return (
                    <div className="admin-controls" data-admin-id={value}>
                      <span title="Accept Invite" onClick={this.acceptAdminInvite} className={(invitePending) ? "" : "w3-hide"}>{FontIcons.acceptIcon}</span>
                      <span title="Edit Administration" onClick={this.showAdminView} className={(invitePending || requestPending) ? "w3-hide" : ""}>{FontIcons.editIcon}</span>
                      <span title="Delete Administration" onClick={this.deleteAdmin}>{FontIcons.deleteIcon}</span>
                      <span className={(invitePending) ? "pending-status" : "w3-hide"}>(Invitation Waiting)</span>
                      <span className={(requestPending) ? "pending-status" : "w3-hide"}>(Request Waiting)</span>
                      <RowLoadingSpinner isShowing={!!row.isWorking}/>
                    </div>
                )
                }} />
                <TableHeaderRow />
                <SelectionState />
                <TableSelection highlightRow={true}
                                selectByRowClick={true}
                                showSelectAll={false}
                                showSelectionColumn={false}/>
              </Grid>
            </div>
          </div>
        )
        :
        (
          <div className="admin-edit-vw">
            <h1 className="admin-hdr">{this.state.currentAdmin.name + " Administration"}</h1>
            <div className="admin-nav">
              <span id="adminShowUsers"
                    className={"admin-nav-link" + ((this.state.currentAdminView === "user") ? " active" : "")}
                    onClick={this.changeAdminView}>Users</span>
              <span id="adminShowDuties"
                    className={"admin-nav-link" + ((this.state.currentAdminView === "duty") ? " active" : "")}
                    onClick={this.changeAdminView}>Duties</span>
              <span id="adminShowSched"
                    className={"admin-nav-link" + ((this.state.currentAdminView === "schedule") ? " active" : "")}
                    onClick={this.changeAdminView}>Schedules</span>
            </div>
            {adminView}
          </div>
        )}
        {(this.props.loading) ? (<LoadingSpinner />) : ""}
      </div>
    )
  }
}

export default withTracker(({viewType: viewType}) => {
  let loading

  if (Meteor.user()) {
    let owner = Meteor.user()._id
    const adminHandle = Meteor.subscribe('admins', owner)
    loading = !adminHandle.ready()
  } else {
    loading = true
  }

  return {
    loading: loading,
    viewType: viewType
  }
})(AdminsView)
