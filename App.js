import React, { Component } from "react"
import { Meteor } from 'meteor/meteor'
import FontIcons from "./fonts"
//import logo from "./img/logo.svg"
//import logo from "./img/RosterMeLogo_white.png"
//import logo from "/images/RosterMeLogo_white.png"
import "./styles/App.scss";
//import MyRostersView from "./myRostersView"
import AdminsSearch from "./adminsSearch"
import AdminsView from "./adminsView"
import { withTracker } from "meteor/react-meteor-data"
import PropTypes from "prop-types"
import LoadingSpinner from './loadingSpinner'
import { SignIn, SignInView } from './signIn'
import SignUp from './signUp'
import HomePage from './homepage'
import Profile from './profile'
import '../api/routes'

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      currentView: "myAdmins",
      isSignedIn: !!Meteor.userId(),
      isSignInShowing: false,
      isSignUpShowing: false,
      signInErred: false
    }

    this.showAdminsSearch = this.showAdminsSearch.bind(this)
    this.showMyAdminsView = this.showMyAdminsView.bind(this)
    this.showJoinedAdminsView = this.showJoinedAdminsView.bind(this)
    this.showSettingsView = this.showSettingsView.bind(this)
    this.showSignIn = this.showSignIn.bind(this)
    this.showSignUp = this.showSignUp.bind(this)
    this.initAuthViews = this.initAuthViews.bind(this)
    //this.signIn = this.signIn.bind(this)
    this.signUp = this.signUp.bind(this)
    this.logOut = this.logOut.bind(this)
  }

  showAdminsSearch () {
    this.setState({currentView: "adminsSearch"});
  }

  showMyAdminsView () {
    this.setState({currentView: "myAdmins"});
  }

  showJoinedAdminsView () {
    this.setState({currentView: "joinedAdmins"});
  }

  showSettingsView () {
    this.setState({currentView: "settings"});
  }

  showSignIn () {
    this.setState({
      currentView: "myAdmins",
      isSignInShowing: true,
      isSignUpShowing: false
    })
  }

  showSignUp () {
    this.setState({
      currentView: "myAdmins",
      isSignInShowing: false,
      isSignUpShowing: true
    })
  }

  initAuthViews () {
    this.setState({
      isSignInShowing: false,
      isSignUpShowing: false
    })
  }

  /*
  signIn (profile) {
    let signInPromise = new Promise((resolve, reject) => {
      Meteor.loginWithPassword(profile.usr, profile.pwd, err => {
        if (err) {
          console.error("Login failed for user, " + profile.usr + ".")
        } else {
          console.log("Logged in successfully! User is: " + Meteor.userId())
        }

        this.setState({
          isSignedIn: !!Meteor.userId(),
          isSignInShowing: !!err,
          signInErred: !!err
        })

        if (err) reject()
        else resolve()
      })
    })

    return signInPromise
  }
  */

  signUp (profile) {
    let app = this
    let username = profile.usr,
        email = profile.email,
        password = profile.pwd

    delete profile.usr
    delete profile.email
    delete profile.pwd

    return new Promise((resolve, reject) => {
      Accounts.createUser({
        username: username,
        email: email,
        password: password,
        profile: profile
      }, (err) => {
        if (err) {
          console.log(err)
          reject(err)
        } else {
          resolve()
          console.log("successfully created user!")
        }
      })
    })
  }

  logOut () {
    Meteor.logout((err) => {
      if (err) {
        console.error(err)
      } else {
        this.setState({
          isSignedIn: !!Meteor.userId()
        })
      }
    })
  }

  render() {
    let view = (<LoadingSpinner />)
    let showControls = false

    if (this.props.loading) {
      view = (<LoadingSpinner />)
    } else if (Meteor.userId() && !this.props.loading) {
      switch (this.state.currentView) {
        case "adminsSearch":
          view = (<AdminsSearch />)
          break;
        case "myAdmins":
          view = (<AdminsView viewType="owned"/>);
          break;
        case "joinedAdmins":
          view = (<AdminsView viewType="joined"/>);
          break;
        case "settings":
          view = (<Profile />);
      }
      //view = (<MyAdminsView />)
    } else {
      //view = (this.state.isSignInShowing) ? (<SignInView submit={this.signIn}/>) : (<HomePage app={this}/>)
      view = (<HomePage app={this} showSignIn={this.state.isSignInShowing} showSignUp={this.state.isSignUpShowing}/>)
      showControls = true
    }

    return (
      <div className="App w3-container">
        <header className="App-header">
          <div className="App-logo" onClick={this.initAuthViews}>
            <img src="/images/RosterMeLogo_white.png" alt="logo" />
          </div>
          {/*<h1 className="App-title">RosterMe</h1>*/}
          { (showControls) ? (
            <div className="login-controls">
              {/*<SignUp submit={this.signUp} app={this}/>
              <SignIn app={this} hide={(this.state.isSignInShowing)}/>*/}
              <button className="sign-up-button" onClick={this.showSignUp}>Register</button>
              <button className="sign-in-button" onClick={this.showSignIn}>Sign In</button>
            </div>) : ""
          }
        </header>
        {/*
        <div id="topNav" className="w3-bar">
          {Meteor.user() ? (
          <div className={(this.state.currentView === "myAdmins") ? "w3-bar-item w3-button active" : "w3-bar-item w3-button"} onClick={this.showMyAdminsView}>
						My Rosters
          </div>) : ""}
          {Meteor.user() ? (<div className={(this.state.currentView !== "myAdmins") ? "w3-bar-item w3-button active" : "w3-bar-item w3-button"} onClick={this.showJoinedAdminsView}>
						Participating Rosters
          </div>) : ""}
          {Meteor.user() ? (
            <Profile logOut={this.logOut}/>
          ) : (
            <div>
              <SignIn submit={this.signIn}/>
              <SignUp submit={this.signUp}/>
            </div>
          )}
        </div>
        */}
        <div className={"w3-sidebar w3-bar-block" + ((this.state.isSignedIn) ? "" : " w3-hide")}>
          <div  className={"w3-bar-item w3-button w3-tooltip" + ((this.state.currentView === "adminsSearch") ? " active" : "")}
                onClick={this.showAdminsSearch}>
            <div className="w3-display-container">
              {FontIcons.search}
            </div>
            <div className="w3-text">Search Admins</div>
          </div>
          <div
              className={"w3-bar-item w3-button w3-tooltip" + ((this.state.currentView === "myAdmins") ? " active" : "")}
              onClick={this.showMyAdminsView}>
            <div className="w3-display-container">
              {FontIcons.myAdmins}
            </div>
            <div className="w3-text">My Admins</div>
          </div>
          <div
              className={"w3-bar-item w3-button w3-tooltip" + ((this.state.currentView === "joinedAdmins") ? " active" : "")}
              onClick={this.showJoinedAdminsView}>
            <div className="w3-display-container">
              {FontIcons.joinedAdmins}
            </div>
            <div className="w3-text">Joined Admins</div>
          </div>
          <div
              className={"w3-bar-item w3-button w3-tooltip" + ((this.state.currentView === "settings") ? " active" : "")}
              onClick={this.showSettingsView}>
            <div className="w3-display-container">
              {FontIcons.settings}
            </div>
            <div className="w3-text">Settings</div>
          </div>
          <div className="w3-bar-item w3-button w3-tooltip" onClick={this.logOut}>
            <div className="w3-display-container">
              {FontIcons.logout}
            </div>
            <div className="w3-text">Logout</div>
          </div>
        </div>
        <div id="mainView" className="w3-display-container" style={((this.state.isSignedIn) ? {marginLeft: "3%"} : {})}>
          {view}
        </div>
      </div>
    );
  }
}

//export default App;
export default withTracker(() => {
  const rosterHandle = Meteor.subscribe('rosters')
  const loading = !rosterHandle.ready()

  return {
    loading: loading
  };
})(App)
