import React from "react";
import ReactDOM from "react-dom";
import RosterInput from "./rosterInput";
import RosterListItem from "./rosterListItem";
import fontIcons from "./fonts";

class MyRostersList extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      rosters: this.props.rosters,
      isNewRosterInputShowing: false
    };
    this.listRosters = this.listRosters.bind(this);
    this.saveRoster = this.saveRoster.bind(this);
    this.showNewRosterInput = this.showNewRosterInput.bind(this);
    this.cancelSave = this.cancelSave.bind(this);
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.props.rosters != prevProps.rosters) {
      this.setState({rosters: this.props.rosters});
    }
  }

  listRosters (roster, idx) {
    let clickHandler = function () {
      this.props.clickHandler(roster._id);
    }.bind(this);

    let active = (this.props.activeRoster) ? (this.props.activeRoster._id === roster._id) : false;

    return (
      <RosterListItem key={roster._id}
        active={active}
        onClick={clickHandler}
        roster={roster}
        remove={this.props.removeRoster}
      />
    );
  }

  showNewRosterInput () {
    this.setState({isNewRosterInputShowing: true});
  }

  saveRoster (inputVal) {
    //hide text box
    this.setState({isNewRosterInputShowing: false});

    //Add new roster to list
    this.props.addRoster(inputVal);
  }

  cancelSave () {
    this.setState({isNewRosterInputShowing: false});
  }

  render () {
    let hide = (this.state.isNewRosterInputShowing) ? "w3-hide" : "";

    return (
      <ul>
        <button onClick={this.showNewRosterInput}
          className={hide}
        >{fontIcons.addIcon}New Roster</button>
        <RosterInput 	showing={this.state.isNewRosterInputShowing}
          save={this.saveRoster}
          cancel={this.cancelSave}
        />
        {this.state.rosters.map(this.listRosters)}
      </ul>
    );
  }
}

export default MyRostersList;
