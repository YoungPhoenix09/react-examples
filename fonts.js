import React from "react";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import solid from "@fortawesome/fontawesome-free-solid";
import regular from "@fortawesome/free-regular-svg-icons";

let fontIcons =  {
  editIcon: <FontAwesomeIcon icon={solid.faEdit} className="" />,
  deleteIcon: <FontAwesomeIcon icon={solid.faTrashAlt} className="" />,
  addIcon: <FontAwesomeIcon icon={solid.faPlus} className="w3-small" />,
  checkIcon: <FontAwesomeIcon icon={solid.faCheck} className="w3-small" />,
  checkBoxIcon: <FontAwesomeIcon icon={solid.faCheck} className="w3-small w3-display-middle" />,
  inviteIcon: <FontAwesomeIcon icon={solid.faUserPlus} className="w3-small" />,
  profileIcon: <FontAwesomeIcon icon={solid.faUserCircle} className="fa-2x" />,
  notify: <FontAwesomeIcon icon={regular.faEnvelopeOpen} className="w3-display-middle primary-dark" />,
  list: <FontAwesomeIcon icon={solid.faTasks} className="w3-display-middle primary-dark" />,
  user: <FontAwesomeIcon icon={solid.faUser} className="mini primary-dark" />,
  users: <FontAwesomeIcon icon={solid.faUsers} className="mini primary-dark" />,
  logout: <FontAwesomeIcon icon={solid.faSignOutAlt} className="fa-2x w3-display-middle" />,
  search: <FontAwesomeIcon icon={solid.faSearch} className="fa-2x w3-display-middle" />,
  myAdmins: <FontAwesomeIcon icon={regular.faFileAlt} className="fa-2x w3-display-middle" />,
  joinedAdmins: <FontAwesomeIcon icon={regular.faListAlt} className="fa-2x w3-display-middle" />,
  acceptIcon: <FontAwesomeIcon icon={regular.faCheckCircle} className="" />,
  settings: <FontAwesomeIcon icon={solid.faCogs} className="fa-2x w3-display-middle" />,
  copyright: <FontAwesomeIcon icon={regular.faCopyright} className="copyright" />
}

export default fontIcons;
