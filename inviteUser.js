import React from "react"
import { withTracker } from "meteor/react-meteor-data"
import fontIcons from './fonts'
import Admin from '../api/administrations'
import { SelectionState, DataTypeProvider, SearchState, IntegratedFiltering } from '@devexpress/dx-react-grid'
import { Grid, Table, TableHeaderRow, TableSelection, SearchPanel, Toolbar } from '@devexpress/dx-react-grid-material-ui'
import "./styles/inviteUser.scss"

class TableCell extends React.Component {
  constructor (props) {
    super(props)
    this.rowClick = this.rowClick.bind(this)
  }

  rowClick() {
    //Do nothing if
    if (this.props.row.added) return

    if (this.props.cellOnClick) {
      //Pass in cell props
      this.props.cellOnClick(this.props)
    }
  }

  render () {
    let value = this.props.value
    let usrName = value.substr(value.indexOf("("))
    let fullName = value.replace(" " + usrName, "")

    return (
      <Table.Cell className={"usr-cell" + ((this.props.row.added) ? " added-user" : "")}>
        <div className="usr-display-name" onClick={this.rowClick}>
          <span className="usr-full-name">{fullName}</span>
          <span className="usr-name">{usrName}</span>
        </div>
      </Table.Cell>
    )
  }
}

class InviteUser extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      inputVal: "",
      showErr: false,
      errMsg: "",
      usrList: [],
      selectedUsers: [],
      showAddInvitedUser: false,
      invitedEmailVal: "",
      isInvitedEmailValid: true,
      invitedEmailValidationMsg: "",
      canAddInvitedUser: false,
      canSendInvite: false
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.sendMail = this.sendMail.bind(this)
    this.sendInvite = this.sendInvite.bind(this)
    this.closeButton = this.closeButton.bind(this)
    this.addSelection = this.addSelection.bind(this)
    this.removeSelection = this.removeSelection.bind(this)
    this.validateFields = this.validateFields.bind(this)
    this.showValidationMsgs = this.showValidationMsgs.bind(this)
    this.addInvitedUser = this.addInvitedUser.bind(this)
  }

  componentDidUpdate (prevProps) {
    //Exit if usrs has not changed
    if (prevProps.usrs.length === this.props.usrs.length) return

    let usrs = this.props.usrs
    let usrList = []

    //Create list to be used in search results grid
    if (usrs.length > 0) {
      //Remove current user from search list
      let currUsrIdx = usrs.indexOf(usrs.filter((usr) => usr._id === Meteor.userId() )[0])
      if (currUsrIdx > -1) usrs.splice(currUsrIdx, 1)

      usrList = usrs.map((usr) => {
        //Try to find usr that is added to the administration
        let addedUser = this.props.usersPage.state.joinedAdminUsrs.filter((joinedUsr) => joinedUsr._id === usr._id)[0]

        return {
          id: usr._id,
          displayName: usr.profile.fname + " " + usr.profile.lname + " (" + usr.username + ")",
          name: usr.profile.fname + " " + usr.profile.lname,
          added: !!addedUser
        }
      })
    }

    this.setState({usrList: usrList})
  }

  handleInputChange (event) {
    let component = this
    let evtTarget = event.target

    if (evtTarget.id === "userField") {
      this.setState({invitedEmailVal: evtTarget.value}, this.validateFields)
    } else {
      this.setState({inputVal: evtTarget.value}, this.validateFields)
    }
  }

  async validateFields () {
    let msg

    let activateBtns = () => {
      this.setState({
        canAddInvitedUser: (this.state.invitedEmailVal && !this.state.invitedEmailValidationMsg)
      })
    }

    let emailValidate = new Promise((resolve, reject) => {
      if (this.state.invitedEmailVal) {
        Meteor.call("signUp.validateField", "email", this.state.invitedEmailVal, (err, result) => {
          if (err) {
            console.log(err)
            reject("Uh oh, something's wrong!  Please try again later.")
          } else {
            resolve(result)
          }
        })
      } else {
        resolve("")
      }
    })

    msg = await emailValidate
    this.setState({
      invitedEmailValidationMsg: msg,
      isInvitedEmailValid: true
    }, activateBtns)
  }

  showValidationMsgs () {
    this.setState({
      isInvitedEmailValid: !this.state.invitedEmailValidationMsg
    })
  }

  addInvitedUser () {
    let invitedUser = {
      id: this.state.invitedEmailVal,
      displayName: this.state.invitedEmailVal,
      name: this.state.invitedEmailVal
    }
    let selectedUsers = this.state.selectedUsers

    //Check if user is already in
    //Push new user into selection of users
    let foundUser = selectedUsers.filter((usr) => usr.displayName === invitedUser.displayName)[0]

    if (!foundUser) {
      selectedUsers.push(invitedUser)
    }

    //Add user and go back to selection page
    this.setState({
      selectedUsers: selectedUsers,
      showAddInvitedUser: false,
      invitedEmailVal: "",
      isInvitedEmailValid: true,
      invitedEmailValidationMsg: "",
      canAddInvitedUser: false,
      canSendInvite: true
    })
  }

  addSelection (cellProps) {
    //console.log(cellProps)

    let selectedUsers = this.state.selectedUsers
    selectedUsers.push(cellProps.row)

    this.setState({
      selectedUsers: selectedUsers,
      canSendInvite: true
    })
  }

  removeSelection (evt) {
    let selectionId = evt.currentTarget.dataset.selectionId
    let selectedUsers = this.state.selectedUsers
    let selection = selectedUsers.filter((usr) => usr.id === selectionId)[0]
    let selectionIdx = selectedUsers.indexOf(selection)

    //Remove selection
    selectedUsers.splice(selectionIdx, 1)
    this.setState({
      selectedUsers: selectedUsers,
      canSendInvite: selectedUsers.length > 0
    })
  }

  sendMail (usr) {
    let errHandler = (err) => {
      //Close dialog
      this.props.cancelAction()

      this.props.usersPage.setState({
        statusMsg: err,
        isStatusErr: true
      })
    }
    let sendPromise = new Promise((resolve, reject) => {
      Meteor.call("invite.sendEmail", [usr.id, this.props.admin.name], (err) => {
        //Add to list of users if successful
        if (!err) {
          resolve()
        } else {
          reject(err)
        }
      })
    })
    .then(() => {
      //Send Mail Success Handler
      let updatePromise = new Promise((resolve, reject) => {
        let updatedAdmin = this.props.admin
        let usrObj = {
          userEmail: this.state.inputVal,
          isSuper: false,
          isPending: true,
          isRejected: false,
          joinMethod: 'invite'
        }

        if (updatedAdmin.users) {
          updatedAdmin.users.push(usrObj)
        } else {
          updatedAdmin.users = [usrObj]
        }

        //Update Admin
        Admin.update(this.props.admin._id, {$set: updatedAdmin}, (err) => {
          if (!err) {
            resolve()
          } else {
            reject(err)
          }
        })
      })

      return updatePromise
    }, (err) => {
      //Send Mail Err Handler
      errHandler(err)
    })
    .then(() => {
      //Update Success Handler
      this.props.usersPage.setState({
        statusMsg: "Request Sent!"
      })

      //Close dialog
      this.closeButton()

    }, (err) => {
      //Update Err Handler
      errHandler(err)
    })
  }

  async sendInvite () {
    let selectedUsers = this.state.selectedUsers
    let erredUsrs = []
    let idx
    let usr
    let callPromise
    let callPromiseResult
    let errMsg
    let adminObj
    let adminList
    let foundAdmin
    let usrObj
    let component = this

    for (idx in selectedUsers ) {
      usr = selectedUsers[idx]

      //Is this a registered user or invited user?
      if (!!usr.id.match(/^[A-Za-z0-9]{17}$/)) {
        //Is a registered user
        //Check if admin is found with user
        if (usr.admins && usr.admins.length > 0) {
          foundAdmin = usr.admins.filter((usrAdmin) => usrAdmin._id === this.props.admin._id )[0]
        }

        if (!foundAdmin) {
          callPromise = new Promise((resolve, reject) => {
            Meteor.call("admin.setAdminUserRelationship", this.props.admin._id, usr.id, 'invite', (err, result) => {
              if (result) {
                //Add to list of erred users
                erredUsrs.push(usr);

                if (!errMsg) {
                  errMsg = "The following users we not able to be requested: " + usr.name
                } else {
                  errMsg = errMsg + ", " + usr
                }
              } else {
                component.props.usersPage.setState({usersUpdated: false})
              }

              resolve()
            })
          })
        }
      } else {
        //Is an invited user
        callPromise = new Promise((resolve, reject) => {
          Meteor.call("usrInviteRequests.newRequest", usr.id, this.props.admin, (err, result) => {
            if (result) {
              //Add to list of erred users
              erredUsrs.push(usr);

              if (!errMsg) {
                errMsg = "The following users we not able to be requested: " + usr.name
              } else {
                errMsg = errMsg + ", " + usr
              }
            } else {
              component.props.usersPage.setState({usersUpdated: false})
            }

            resolve()
          })
        })
      }

      callPromiseResult = await callPromise
    }

    //Show err message if err present
    if (erredUsrs.length > 0) {
      this.setState({
        showErr: true,
        errMsg: errMsg
      })
    } else {
      //Update Success Handler
      this.props.usersPage.setState({
        statusMsg: "Request(s) Sent!",
        usersUpdated: false
      })

      //Close dialog
      this.closeButton()
    }
  }

  closeButton () {
    this.setState({
      inputVal: "",
      showErr: false,
      errMsg: "",
      selectedUsers: [],
      showAddInvitedUser: false,
      invitedEmailVal: "",
      isInvitedEmailValid: true,
      invitedEmailValidationMsg: "",
      canAddInvitedUser: false,
      canSendInvite: false
    })
    this.props.cancelAction()
  }

  render () {
    let hide = (this.props.showDialog) ? "w3-show" : "";
    let filterPredicate = (value, filter) => {
      //Check if filter value is found in value and value is not in list of selected users
      return value.toLowerCase().indexOf(filter.value.toLowerCase()) > -1 && !this.state.selectedUsers.filter( (usr) => usr.displayName === value )[0]
    }
    let tableCellComponent = (props) => (
      <TableCell {...props} cellOnClick={this.addSelection}/>
    )
    let showAddInvitedUser = () => {
      this.setState({
        invitedEmailVal: this.state.inputVal,
        showAddInvitedUser: true,
        invitedEmailValidationMsg: ""
      }, this.validateFields)
    }
    let tableNoDataCellComponent = (props) => (
      <td className="no-user-data">
        <div>No Other Matching Users</div>
        <button className="auxiliary w3-round-large"
                onClick={showAddInvitedUser}
        >
          Request User
        </button>
      </td>
    )

    //Default view
    let view = (
      <div>
        <div className="selected-users-section">
          {this.state.selectedUsers.map((usr) => (
            <span key={usr.id} className="selection-item">
              {usr.name}
              <span className="selection-item-remove"
                    data-selection-id={usr.id}
                    onClick={this.removeSelection}
              >X</span>
            </span>
          ))}
        </div>
        <div className="search-container">
          <div className="search-box">
            <input  type="text"
                    id="searchField"
                    value={this.state.inputVal}
                    placeholder="Search by name or user name"
                    autoComplete="off"
                    onChange={this.handleInputChange}/>
            <div className="w3-display-container">{fontIcons.search}</div>
          </div>
          <div className={"search-results" + ((this.state.inputVal.length > 2) ? "" : " w3-hide")}>
            <Grid
              rows={this.state.usrList}
              columns={[
                {name: "displayName", title: "Full Name"}
              ]}>
              <SearchState    value={this.state.inputVal} />
              <IntegratedFiltering columnExtensions={[
                {columnName: "displayName", predicate: filterPredicate}
              ]}/>
              <Table cellComponent={tableCellComponent} noDataCellComponent={tableNoDataCellComponent}/>
            </Grid>
          </div>
        </div>
        <div className="control-bar">
          <button
            onClick={this.sendInvite}
            className="primary w3-round-large"
            disabled={!this.state.canSendInvite}
          >Send Invite</button>
        </div>
      </div>
    )

    if (this.state.showErr) {
      view = (
        <div className="err-container">
          <h1>Whoops!</h1>
          <p>{this.state.errMsg}</p>
        </div>
      )
    } else if (this.state.showAddInvitedUser) {
      view = (
        <div>
          <div className="add-user-fields">
            <label>Enter User's Email</label>
            <input  type="text"
                    id="userField"
                    onChange={this.handleInputChange}
                    onBlur={this.showValidationMsgs}
                    value={this.state.invitedEmailVal} />
            <div className={(!this.state.isInvitedEmailValid) ? "validation-msg" : "w3-hide"}>
              {this.state.invitedEmailValidationMsg}
            </div>
          </div>
          <div className="control-bar">
            <button
              onClick={this.addInvitedUser}
              className="add-user-btn primary w3-round-large"
              disabled={!this.state.canAddInvitedUser}
            >Add User</button>
            <button
              onClick={() => { this.setState({showAddInvitedUser: false, inputVal: ""}) }}
              className="back-btn secondary w3-round-large"
            >Go Back</button>
          </div>
        </div>
      )
    }

    return (
      <div className={hide + " w3-modal invited-users-modal"}>
        <div className="w3-modal-content">
          <button
            onClick={this.closeButton}
            className="cls-button"
          >X</button>
          <div className="w3-container">
            <div className="w3-row-padding">
              {view}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTracker(({
  showDialog: showDialog,
  cancelAction: cancelAction,
  sendAction: sendAction,
  admin: admin,
  usersPage: usersPage}) =>
  {
    let owner = Meteor.user()._id
    const usrsHandle = Meteor.subscribe('allUsrs')
    //const adminUsrHandle = Meteor.subscribe('adminUsers', admin)
    const loading = !usrsHandle.ready() //|| !adminUsrHandle.ready()

    return {
      loading: loading,
      admin: admin,
      showDialog: showDialog,
      cancelAction: cancelAction,
      sendAction: sendAction,
      usersPage: usersPage,
      usrs: (loading) ? [] : Meteor.users.find().fetch()
    }
  }
)(InviteUser)
