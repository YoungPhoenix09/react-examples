import React from 'react';
import FontIcons from "./fonts";
import "./styles/profile.css";
import DialogBox from "./dialogBox";

class Profile extends React.Component {
  constructor (props) {
    super(props)

    let usr = Meteor.user()
    let action = () => { this.setState({showDialog: false, dialogMsg: ""}) }

    this.state = {
      showDialog: false,
      dialogMsg: "",
      dialogHasYesNo: false,
      dialogYesAction: action,
      dialogNoAction: action,
      showProfilePopup: false,
      showEdit: false,
      profile: {
        fname: usr.profile.fname,
        lname: usr.profile.lname,
        email: usr.emails[0].address,
        phone: usr.profile.phone,
        oldPword: "",
        pword:"",
        cnfPword: ""
      },
      profileStatMsg: "",
      profileStatMsgErr: false,
      profilePwdMsg: "",
      profilePwdMsgErr: false
    }

    this.showProfilePopup = this.showProfilePopup.bind(this)
    this.showDialog = this.showDialog.bind(this)
    this.hideDialog = this.hideDialog.bind(this)
    this.updateField = this.updateField.bind(this)
    this.updateProfile = this.updateProfile.bind(this)
    this.updatePassword = this.updatePassword.bind(this)
    this.validateUpdateProfile = this.validateUpdateProfile.bind(this)
    this.validateUpdatePassword = this.validateUpdatePassword.bind(this)
    this.confirmAcctDelete = this.confirmAcctDelete.bind(this)
    this.deleteAccount = this.deleteAccount.bind(this)
  }

  showDialog (msg, hasYesNo, yesAction, noAction) {
    let action = () => { this.setState({showDialog: false, dialogMsg: ""}) }

    this.setState({
      dialogMsg: msg || "",
      showDialog: true,
      dialogHasYesNo: hasYesNo || false,
      dialogYesAction: yesAction || action,
      dialogNoAction: noAction || action
    })
  }

  hideDialog () {
    let action = () => { this.setState({showDialog: false, dialogMsg: ""}) }

    this.setState({
      dialogMsg: "",
      showDialog: false,
      dialogHasYesNo: false,
      dialogYesAction: action,
      dialogNoAction: action
    })
  }

  showProfilePopup () {
    let usr = Meteor.user()

    this.setState({
      showProfilePopup: true,
      profile: {
        fname: usr.profile.fname,
        lname: usr.profile.lname,
        email: usr.emails[0].address,
        phone: usr.profile.phone,
        oldPword: "",
        pword: "",
        cnfPword: ""
      },
      profileStatMsg: "",
      profileStatMsgErr: false,
      profilePwdMsg: "",
      profilePwdMsgErr: false
    });
  }

  updateField (e) {
    let profile = this.state.profile

    switch (e.target.id) {
      case "profileFirstName":
        profile.fname = e.target.value
        break;
      case "profileLastName":
        profile.lname = e.target.value
        break;
      case "profileEmail":
        profile.email = e.target.value
        break;
      case "profilePhone":
        profile.phone = e.target.value
        break;
      case "profileOldPwrd":
        profile.oldPword = e.target.value
        break;
      case "profilePwrd":
        profile.pword = e.target.value
        break;
      case "profileCnfrmPwrd":
        profile.cnfPword = e.target.value
    }

    this.setState({
      profile: profile
    })
  }

  validateUpdateProfile () {
    let valid = true;

    let validFname = this.state.profile.fname.match(/^[A-Za-z`^']{1,25}$/)
    let validLname = this.state.profile.lname.match(/^[A-Za-z`^']{1,25}$/)
    let validEmail = this.state.profile.email.match(/^[A-z!#$%&'*\+\-\/=?^_`{|}~;.]{1,30}@[a-z0-9\-]{1,30}.[a-z0-9\-]{1,30}$/)
    let validPhone = this.state.profile.phone.match(/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/)

    if (!validFname) {
      this.setState({
        profileStatMsg: "Invalid first name!",
        profileStatMsgErr: true
      });
      valid = false;
      return
    }

    if (!validLname) {
      this.setState({
        profileStatMsg: "Invalid last name!",
        profileStatMsgErr: true
      });
      valid = false;
      return
    }

    if (!validEmail) {
      this.setState({
        profileStatMsg: "Invalid email address!",
        profileStatMsgErr: true
      });
      valid = false;
      return
    }

    if (!validPhone) {
      this.setState({
        profileStatMsg: "Invalid phone number!",
        profileStatMsgErr: true
      });
      valid = false;
      return
    }

    if (valid) {
      this.updateProfile()

      //Reset error messages
      this.setState({
        profileStatMsg: "",
        profileStatMsgErr: false
      });
    }
  }

  validateUpdatePassword () {
    let valid = true

    //Verify fields are not empty
    if (!this.state.profile.oldPword || !this.state.profile.pword || !this.state.profile.cnfPword) {
      valid = false

      this.setState({
        profilePwdMsg: "Please make sure all fields are completed",
        profilePwdMsgErr: true
      })
      return
    }

    //Check password fits requirements
    let pword = this.state.profile.pword
    if (!pword.match(/[0-9]/) || !pword.match(/[A-z]/) ||
      pword.length < 8 || pword.length > 16) {
      valid = false

      this.setState({
        profilePwdMsg: "New password does not meet strength requirements!",
        profilePwdMsgErr: true
      })
      return
    }

    //Verify new passwords match
    if (this.state.profile.pword !== this.state.profile.cnfPword) {
      valid = false

      this.setState({
        profilePwdMsg: "Password and confirmation password do not match!",
        profilePwdMsgErr: true
      })
      return
    }

    if (valid) {
      this.updatePassword()

      this.setState({
        profilePwdMsg: "",
        profilePwdMsgErr: false
      })
    }
  }

  updateProfile () {
    Meteor.call("profile.updateProfile", Meteor.user(), this.state.profile, (callErr, resp) => {
      if (resp.isErr) {
        this.setState({
          profileStatMsg: resp.msg,
          profileStatMsgErr: resp.isErr
        })
      } else {
        this.showDialog("Profile Updated!", false, this.hideDialog, this.hideDialog)
      }
    })
  }

  updatePassword () {
    let me = this;

    Accounts.changePassword(this.state.profile.oldPword, this.state.profile.pword, (error) => {
      if (error) {
        let errMsg = (error.error === 403) ? "Your old password is incorrect.  Please re-enter old password." : "Can't update password at this time.  Please try again later."
        console.log(error)

        me.setState({
          profilePwdMsg: errMsg,
          profilePwdMsgErr: false
        })
      } else {
        let profile = me.state.profile

        profile.oldPword = ""
        profile.pword = ""
        profile.cnfPword = ""

        me.setState({
          profile: profile
        })

        this.showDialog("Password Updated!", false, this.hideDialog, this.hideDialog)
      }
    });
  }

  confirmAcctDelete () {
    this.showDialog("Are you sure you would to delete your account?", true, this.deleteAccount, this.hideDialog)
  }

  deleteAccount () {
    Meteor.call("main.deleteAccount", this.hideDialog)
  }

  render () {
    let usr = Meteor.user()

    return (
      <div id="profileSection">
        <div id="profile-edit">
          <div className="title w3-xlarge">{usr.username + "'s Account Settings"}</div>
          <div className="acct-fields">
            <div className="w3-row">
              <h4>Profile</h4>
            </div>
            { ( !!this.state.profileStatMsg ) ? (
              <div className={"profile-stat-msg" + ((this.state.profileStatMsgErr) ? " w3-pink" : "")}>
                {this.state.profileStatMsg}
              </div>
            ) : ""}
            <div></div>
            <div className="w3-row">
              <div className="w3-col s3 m1 l1">
                <label htmlFor="profileFirstName">First Name</label>
              </div>
              <div className="w3-rest">
                <input type="text" id="profileFirstName" onChange={this.updateField} value={this.state.profile.fname} />
              </div>
            </div>
            <div className="w3-row">
              <div className="w3-col s1">
                <label htmlFor="profileLastName">Last Name</label>
              </div>
              <div className="w3-rest">
                <input type="text" id="profileLastName" onChange={this.updateField} value={this.state.profile.lname} />
              </div>
            </div>
            <div className="w3-row">
              <div className="w3-col s1">
                <label htmlFor="profileEmail">Email</label>
              </div>
              <div className="w3-rest">
                <input type="email" id="profileEmail" onChange={this.updateField} value={this.state.profile.email} />
              </div>
            </div>
            <div className="w3-row">
              <div className="w3-col s1">
                <label htmlFor="profilePhone">Phone</label>
              </div>
              <div className="w3-rest">
                <input type="text" id="profilePhone" placeholder="###-###-####" onChange={this.updateField} value={this.state.profile.phone} />
              </div>
            </div>
            <button className="w3-btn w3-ripple w3-border w3-border-teal"
                    onClick={this.validateUpdateProfile}>
              Update Profile
            </button>
            <div className="w3-row">
              <h4>Password</h4>
            </div>
            { ( !!this.state.profilePwdMsg ) ? (
              <div className={"profile-stat-msg" + (this.state.profilePwdMsgErr) ? " w3-pink" : ""}>
                {this.state.profilePwdMsg}
              </div>
            ) : ""}
            <div className="w3-row">
              <div className="w3-col s1">
                <label htmlFor="profileOldPwrd">Old Password</label>
              </div>
              <div className="w3-rest">
                <input type="password" id="profileOldPwrd" onChange={this.updateField} value={this.state.profile.oldPword} />
              </div>
            </div>
            <div className="w3-row">
              <div className="w3-col s1">
                <label htmlFor="profilePwrd">New Password</label>
              </div>
              <div className="w3-rest">
                <input type="password" id="profilePwrd" onChange={this.updateField} value={this.state.profile.pword} />
              </div>
            </div>
            <div className="w3-row">
              <div className="w3-col s1">
                <label htmlFor="profileCnfrmPwrd">Confirm Password</label>
              </div>
              <div className="w3-rest">
                <input type="password" id="profileCnfrmPwrd" onChange={this.updateField} value={this.state.profile.cnfPword} />
              </div>
            </div>
            <button className="w3-btn w3-ripple w3-border w3-border-teal"
                    onClick={this.validateUpdatePassword}>
              Update Password
            </button>
          </div>
          <div className="w3-row">
            <h4>Delete Account</h4>
          </div>
          <button className="w3-btn w3-ripple w3-border w3-border-teal"
                  onClick={this.confirmAcctDelete}>
            Delete Account
          </button>
        </div>
        <DialogBox
          msg={this.state.dialogMsg}
          hasYesNo={this.state.dialogHasYesNo}
          showDialog={this.state.showDialog}
          yesAction={this.state.dialogYesAction}
          noAction={this.state.dialogNoAction}
        />
      </div>
    )
  }
}

export default Profile;
