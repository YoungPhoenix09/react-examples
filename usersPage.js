import React from "react"
import { Meteor } from "meteor/meteor"
import { withTracker } from "meteor/react-meteor-data"
import fontIcons from './fonts'
import DialogBox from './dialogBox'
import InviteUser from './inviteUser'
import CheckBox from './checkBox'
import LoadingSpinner from './loadingSpinner'
import MsgView from './msgView'
import UsrMessages from '../api/usrMessages'
import "./styles/usersPage.scss"

class UsersPage extends React.Component {
  constructor (props) {
    super(props)

    //Check if user is a super users
    let user = Meteor.user()
    let userObj

    if (user && this.props.admin.users) {
      userObj = this.props.admin.users.filter((usr) => usr.userEmail === user.emails[0].address)[0] || {}
    } else {
      userObj = {}
    }

    this.state = {
      selectedUser: null,
      showDeleteDialog: false,
      showInviteDialog: false,
      adminMsgInput: "",
      statusMsg: "",
      isStatusErr: false,
      joinedAdminUsrs: [],
      usrListView: "current",
      requestedUsers: [],
      currentUsers: [],
      invitedUsers: [],
      usersUpdated: true,
      isSuperUser: userObj.isSuper || this.props.admin.owner === user._id
    }

    this.changeSelectedUser = this.changeSelectedUser.bind(this)
    this.changeFieldInput = this.changeFieldInput.bind(this)
    this.showInviteDialog = this.showInviteDialog.bind(this)
    this.hideInviteDialog = this.hideInviteDialog.bind(this)
    this.showDeleteDialog = this.showDeleteDialog.bind(this)
    this.hideDeleteDialog = this.hideDeleteDialog.bind(this)
    this.getUsers = this.getUsers.bind(this)
    this.addRmvUser = this.addRmvUser.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    this.clearMessage = this.clearMessage.bind(this)
    this.showCurrentUsrs = this.showCurrentUsrs.bind(this)
    this.showInvitedUsrs = this.showInvitedUsrs.bind(this)
    this.showUsrRequests = this.showUsrRequests.bind(this)
    this.removeUser = this.removeUser.bind(this)
    this.updateJoinedUsers = this.updateJoinedUsers.bind(this)
    this.approveUser = this.approveUser.bind(this)
    this.rejectUser = this.rejectUser.bind(this)
  }

  componentDidMount() {
    this.updateJoinedUsers()
  }

  componentDidUpdate (prevProps, prevState) {
    if (!this.state.usersUpdated) {
      this.updateJoinedUsers()
    }
  }

  updateJoinedUsers () {
    let component = this
    let currentUsers = []
    let invitedUsers = []
    let requestedUsers = []

    //Set now to make sure anything down the line doesn't retrigger this function
    component.setState({usersUpdated: true})

    Meteor.call("admin.getAdminUsrs", this.props.admin, (err, usrs) => {
      component.setState({
        joinedAdminUsrs: usrs || []
      }, () => {
        component.state.joinedAdminUsrs.forEach((usr) => {
          if (usr.joinedAdminProps[0].isPending) {
            if (usr.joinedAdminProps[0].joinMethod === "invite") {
              invitedUsers.push(usr)
            } else {
              requestedUsers.push(usr)
            }
          } else {
            currentUsers.push(usr)
          }
        })

        //Add owner to list if owner isn't current user
        if (component.props.admin.owner !== Meteor.userId()) {
          let ownerUserObj = Meteor.users.findOne(component.props.admin.owner)
          ownerUserObj.joinedAdminProps = {
            isSuper: true,
            isPending: false,
            isRejected: false,
            joinMethod: "owner"
          }

          currentUsers.push(ownerUserObj)
        }

        component.setState({
          invitedUsers: invitedUsers,
          currentUsers: currentUsers,
          requestedUsers: requestedUsers
        })
      })
    })
  }

  getUsers (user, idx) {
    this.setState({
      users: Meteor.users.find().fetch()
    })
  }

  changeSelectedUser (event) {
    let selectedUsrList

    switch (this.state.usrListView) {
      case "current":
        selectedUsrList = "currentUsers"
        break
      case "invited":
        selectedUsrList = "invitedUsers"
        break
      case "requests":
        selectedUsrList = "requestedUsers"
    }

    let selectedUser = null

    //Only get user if the element containing the username is clicked and not a child element
    if (event.currentTarget === event.target) {
      selectedUser = this.state[selectedUsrList].filter( d => d._id === event.currentTarget.dataset.userId)[0]
    }


    if (this.state.selectedUser !== selectedUser) {
      this.setState({selectedUser: selectedUser})
    } else {
      this.setState({selectedUser: null})
    }
  }

  changeFieldInput (event) {
    let newVal = event.target.value

    if (event.target.id === "msgInputField") {
      this.setState({adminMsgInput: newVal})
    }

  }

  showInviteDialog () {
    this.setState({showInviteDialog: true})
  }

  hideInviteDialog () {
    this.setState({showInviteDialog: false})
  }

  showDeleteDialog () {
    this.setState({showDeleteDialog: true})
  }

  hideDeleteDialog () {
    this.setState({showDeleteDialog: false})
  }

  showCurrentUsrs () {
    this.setState({usrListView: "current"})
  }

  showInvitedUsrs () {
    this.setState({usrListView: "invited"})
  }

  showUsrRequests () {
    this.setState({usrListView: "requests"})
  }

  addRmvUser (user) {
    let duty = this.state.currentDuty
    let assignedUsers = duty.assignedUsers
    let userIdx = assignedUsers.indexOf(user.id)

    if (userIdx > -1) {
      //Remove user from assigned
      assignedUsers.splice(userIdx, 1)
    } else {
      //Add user to assigned
      assignedUsers.push(user.id)
    }

    duty.assignedUsers = assignedUsers
    this.setState({currentDuty: duty})
  }

  approveUser (evt) {
    let userId = evt.currentTarget.dataset.userId
    let adminId = this.props.admin._id
    let component = this

    Meteor.call("admin.approvePending", adminId, userId, () => {
      component.setState({usersUpdated: false})
    })
  }

  rejectUser (evt) {
    let userId = evt.currentTarget.dataset.userId
    let adminId = this.props.admin._id
    let component = this

    /*Meteor.call("admin.rejectRequest", adminId, userId, () => {
      component.setState({usersUpdated: false})
    })*/

    Meteor.call("admin.removeAdminUserRelationship", adminId, userId, () => {
      component.setState({usersUpdated: false})
    })
  }

  removeUser (evt) {
    let userId = evt.currentTarget.dataset.userId
    let adminId = this.props.admin._id
    let component = this

    Meteor.call("admin.removeAdminUserRelationship", adminId, userId, () => {
      component.setState({usersUpdated: false})
    })
  }

  sendMessage () {
    let adminId = this.props.admin._id,
        senderId = Meteor.user()._id,
        recipient = null,
        msg = this.state.adminMsgInput

    Meteor.call("usrMsgs.sendMessage", adminId, senderId, recipient, msg)
    this.clearMessage()
  }

  clearMessage () {
    this.setState({adminMsgInput: ""})
  }

  render () {
    let selectedUsrList

    switch (this.state.usrListView) {
      case "current":
        selectedUsrList = "currentUsers"
        break
      case "invited":
        selectedUsrList = "invitedUsers"
        break
      case "requests":
        selectedUsrList = "requestedUsers"
    }

    let msgUsr = ""
    if (this.state.selectedUser) {
      let usrFullName = `${this.state.selectedUser.profile.fname} ${this.state.selectedUser.profile.lname}`
      msgUsr = ` (to ${usrFullName})`
    }

    return (
      <div className="users-section">
        <div className={((this.state.statusMsg) ? "status-msg" : "w3-hide")}>
          {this.state.statusMsg}
        </div>
        <div className="w3-col s4 users-pane">
          <span className="w3-left-align users-section-hdr">Users</span>
          <button
            className={(this.state.isSuperUser) ? "invite-user-btn": "w3-hide"}
            onClick={this.showInviteDialog}
          >
            {fontIcons.inviteIcon}Invite User
          </button>
          <div className="admin-users-view">
            <div className={(this.state.isSuperUser) ? "viewTglBar" : "w3-hide"}>
              <div  onClick={this.showCurrentUsrs}
                    className={"usr-vw-tgl" + ((this.state.usrListView === "current") ? " active" : "")}
              >
                Current
              </div>
              <div  onClick={this.showInvitedUsrs}
                    className={"usr-vw-tgl" + ((this.state.usrListView === "invited") ? " active" : "")}
              >
                Invited
              </div>
              <div  onClick={this.showUsrRequests}
                    className={"usr-vw-tgl" + ((this.state.usrListView === "requests") ? " active" : "")}
              >
                Requests
              </div>
            </div>
            {(this.state.joinedAdminUsrs.length) ? (
              (this.state[selectedUsrList].length > 0) ? (
                this.state[selectedUsrList].map(((usr) => {
                  if (usr._id !== Meteor.userId()) {
                    return (
                      <p  key={usr._id}
                          className={"user-item w3-hover-light-grey" + ((this.state.usrListView === "current") ? "" : " pending") }
                          data-user-id={usr._id}
                          onClick={this.changeSelectedUser}
                      >
                        {usr.profile.fname + " " + usr.profile.lname}
                        <span onClick={(selectedUsrList === "requestedUsers") ? this.rejectUser : this.removeUser}
                              title={(selectedUsrList === "requestedUsers") ? "Reject User" : "Remove User"}
                              data-user-id={usr._id}
                              className={(this.state.isSuperUser) ? "" : "w3-hide"}>X</span>
                        <span className={(selectedUsrList === "requestedUsers") ? "approve-usr-btn" : "w3-hide"}
                              data-user-id={usr._id}
                              title="Approve User"
                              onClick={this.approveUser}>
                          {fontIcons.checkIcon}
                        </span>
                      </p>
                    )
                  }
                }).bind(this))
              )
              :
              (
                <p className="no-users">NO USERS</p>
              )
            )
            :
            (
              <p className="no-users">NO USERS</p>
            )}
          </div>
        </div>
        <div className="w3-col w3-half msgs-pane">
          <span className="w3-left-align users-section-hdr">Messages</span><span className="w3-left-align msg-user">{msgUsr}</span>
          <div className="admin-messages">
            <MsgView msgs={this.props.msgs}/>
            <div className="w3-row">
              <textarea id="msgInputField"
                        className="w3-twothird admin-messages-input"
                        value={this.state.adminMsgInput}
                        onChange={this.changeFieldInput}/>
              <div className="w3-rest admin-messages-controls">
                <button disabled={!this.state.adminMsgInput} onClick={this.sendMessage}>√</button>
                <button onClick={this.clearMessage}>X</button>
              </div>
            </div>

          </div>
        </div>
        <DialogBox
          msg="Are you sure you want to delete this duty?"
          hasYesNo={true}
          showDialog={this.state.showDeleteDialog}
          yesAction={this.removeDuty}
          noAction={this.hideDeleteDialog}
        />
        <InviteUser
          showDialog={this.state.showInviteDialog}
          cancelAction={this.hideInviteDialog}
          sendAction={this.inviteUser}
          admin={this.props.admin}
          usersPage={this}
        />
        {(this.props.loading) ? (<LoadingSpinner />) : ""}
      </div>
    )
  }
}

export default withTracker(({admin: admin}) => {
  let owner = Meteor.user()._id
  const usrMsgHandle = Meteor.subscribe('usrMsgs', owner, admin._id)
  //const adminUsrHandle = Meteor.subscribe('adminUsers', admin)
  const loading = !usrMsgHandle.ready() //|| !adminUsrHandle.ready()

  return {
    loading: loading,
    admin: admin,
    msgs: (loading) ? [] : UsrMessages.find().fetch()/*,
    users: (loading) ? [] : Meteor.users.find().fetch()*/
  }
})(UsersPage)
