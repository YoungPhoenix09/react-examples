import React from "react";
import ReactDOM from "react-dom";
import './styles/dialogBox.scss';

class DialogBox extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    let hide = (this.props.showDialog) ? "w3-show " : "";
    let btns;

    if (this.props.hasYesNo) {
      btns = (
        <div className="w3-bar">
          <button onClick={this.props.yesAction}
            className="primary w3-round-large"
          >Yes</button>
          <button onClick={this.props.noAction}
            className="secondary w3-round-large"
          >No</button>
        </div>
      );
    } else {
      btns = (
        <div className="w3-bar">
          <button onClick={this.props.yesAction}
            className="primary w3-round-large"
          >Okay</button>
        </div>
      );
    }

    return (
      <div className={hide + "dialog w3-modal"}>
        <div  className="w3-modal-content w3-round w3-card-4 w3-animate-top"
          style={{maxWidth: "30%"}}
        >
          <p>
            {this.props.msg}
          </p>
          {btns}
        </div>
      </div>
    );
  }
}

export default DialogBox;
