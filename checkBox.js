import React from 'react';
import fontIcons from './fonts';
import './styles/checkbox.css';

class CheckBox extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isChecked: this.props.isChecked || false
    }
  }

  componentDidUpdate (prevProps) {
    if (this.props !== prevProps) {
      this.setState({
        isChecked: this.props.isChecked || false
      });
    }
  }

  render () {
    return (
      <div
        className="checkBox w3-display-container"
        onClick={this.props.onClick}
      >
        {(this.state.isChecked) ? fontIcons.checkBoxIcon : ""}
      </div>
    )
  }
}

export default CheckBox;
