import React from "react";
import ReactDOM from "react-dom";

class RosterUserMainMenu extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      rosters: [{
        name: "Church"
      },{
        name: "Work"
      }]
    };
    this.listRosters = this.listRosters.bind(this);
  }

  listRosters (roster, idx) {
    return (
      <li>{roster.name}</li>
    );
  }

  render () {
    return (
      <div>
        <ul>
          {this.state.rosters.map(this.listRosters)}
        </ul>
      </div>
    );
  }
}

export default RosterUserMainMenu;
