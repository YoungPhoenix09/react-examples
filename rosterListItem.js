import React from 'react';
import fontIcons from './fonts';
import RosterInput from "./rosterInput";

import Rosters from "../api/rosters";

class RosterListItem extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      roster: this.props.roster,
      isEdit: false
    };

    this.editClick = this.editClick.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
    this.update = this.update.bind(this);
    this.remove = this.remove.bind(this);
  }

  editClick (e) {
    //Prevent bubbling up to parent
    e.stopPropagation();

    this.setState({isEdit: true});
  }

  cancelEdit () {
    this.setState({isEdit: false});
  }

  /*
  update (id, rosterName) {
    let updatedRoster = this.props.roster;

    updatedRoster.name = rosterName;

    this.props.roster.updateRoster(updatedRoster);
    this.cancelEdit();
  }
  */
  update (id, rosterName) {
    let roster = Rosters.findOne(id);
    console.log(roster);

    Rosters.update({_id: id}, { $set: {name: rosterName}}, () => {
      this.setState({roster: Rosters.findOne(id)})
    });
    this.cancelEdit();
  }

  remove (e) {
    //Prevent bubbling up to parent
    e.stopPropagation();

    this.props.remove(this.state.roster._id);
  }

  render () {
    let active = (this.props.active) ? "active" : "";
    let hide = (this.state.isEdit) ? "w3-hide" : "";

    return(
      <li className={active + " w3-display-container"}
        onClick={this.props.onClick}
      >
        <div className={hide + " w3-center rosterName"}>{this.state.roster.name}</div>
        <RosterInput  id={this.state.roster._id}
          showing={this.state.isEdit}
          save={this.update}
          cancel={this.cancelEdit}
        />
        <span className={hide + " controls w3-display-right"}>
          <button onClick={this.editClick} title="Rename">{fontIcons.editIcon}</button>
          <button onClick={this.remove} title="Remove">{fontIcons.deleteIcon}</button>
        </span>
      </li>
    )
  }
}

export default RosterListItem;
