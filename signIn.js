import React from 'react'
import './styles/signIn.scss'

class SignIn extends React.Component {

  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div className={"acct" + ((this.props.hide)  ? " w3-hide" : "") }>
        <span className="link sign-in-button" onClick={() => { this.props.app.setState({showSignIn: true}) }}>Sign In</span>
      </div>
    )
  }
}

class SignInView extends React.Component {
  constructor (props) {
    super(props)

    //Method bindings
    this.signIn = this.signIn.bind(this)
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.validateFields = this.validateFields.bind(this)
    this.showValidationMsgs = this.showValidationMsgs.bind(this)

    this.state = {
      signInUser: "",
      signInPwrd: "",
      canSignIn: "",
      usrValidationMsg: "",
      pwdValidationMsg: "",
      isUsrValid: true,
      isPwdValid: true
    }
  }

  handleFieldChange (e) {
    let target = e.currentTarget.id

    let runValidation = () => {
      let inputTrgt = target
      this.validateFields(inputTrgt)
    }

    switch (e.currentTarget.id) {
      case "signInUser":
        this.setState({
          signInUser: e.currentTarget.value
        }, runValidation)
        break
      case "signInPwrd":
        this.setState({
          signInPwrd: e.currentTarget.value
        }, runValidation)
        break
    }
  }

  validateFields (target) {
    let valid
    let msg

    //User can continue if no validation msgs present
    let activateBtns = () => {
      this.setState({
        canSignIn: (this.state.signInUser && !this.state.usrValidationMsg) &&
          (this.state.signInPwrd && !this.state.pwdValidationMsg)
      })
    }

    //Promise for meteor method to validate username
    if (!this.state.showNext) {
      switch (target) {
        case "signInUser":
          //Validate username
          valid = !!this.state.signInUser.match(/^[A-Za-z]\w{5,29}$/) || !!this.state.signInUser.match(/^[A-z0-9](\w|\.)*[A-z0-9]@[A-z0-9](\w|\.)*[A-z0-9]\.[A-z0-9](\w|\.)*[A-z0-9]$/)

          this.setState({
            usrValidationMsg: (valid) ? "" : "Not a valid username or email",
            isUsrValid: true
          }, activateBtns)

          break
        case "signInPwrd":
          //Validate password
          valid = (!!this.state.signInPwrd.match(/^\S{8,16}$/))

          this.setState({
            pwdValidationMsg: (valid) ? "" : "Password is not valid",
            isPwdValid: true
          }, activateBtns)
          break
      }
    }
  }

  showValidationMsgs (e) {
    switch (e.currentTarget.id) {
      case "signInUser":
        this.setState({
          isUsrValid: !this.state.usrValidationMsg
        })
        break
      case "signInPwrd":
        this.setState({
          isPwdValid: !this.state.pwdValidationMsg,
        })
    }
  }

  signIn (e) {
    //Stop refresh
    e.preventDefault();

    let profile = {
      usr: this.state.signInUser,
      pwd: this.state.signInPwrd
    }

    Meteor.loginWithPassword(profile.usr, profile.pwd, err => {
      if (err) {
        console.error("Login failed for user, " + profile.usr + ".")

        this.setState({
          showLoginErr: true
        })

        //Hide after 10 seconds
        setTimeout(() => {
          this.setState({
            showLoginErr: false
          })
        }, 10000)
      } else {
        //console.log("Logged in successfully! User is: " + Meteor.userId())

        //Set app state
        this.props.app.setState({
          isSignedIn: true,
          isSignInShowing: false
        })
      }
    })
  }

  render () {
    return (
      <div ref={this.props.innerRef} className={"signIn w3-container w3-display-middle"}>
        <div className={"login-err" + ((this.state.showLoginErr) ? " shown" : "")}>
          Invalid login!  Please try again.
        </div>
        <h2>Sign In</h2>
        <div className="sign-in-content">
          <form onSubmit={this.signIn}>
            <div className="form-fields">
              <div className="field">
                <p><label htmlFor="signInUser">User Name or Email</label></p>
                <p>
                  <input  type="text"
                          className={"w3-input w3-border w3-round" + (this.state.isUsrValid ? "" : " invalid")}
                          id="signInUser"
                          onChange={this.handleFieldChange}
                          onBlur={this.showValidationMsgs}
                          value={this.state.signInUser} />
                </p>
                <p className={(this.state.isUsrValid) ? "w3-hide" : "validation-msg"}>{this.state.usrValidationMsg}</p>
              </div>
              <div className="field">
                <p><label htmlFor="signInPwrd">Password</label></p>
                <p>
                  <input  type="password"
                          className={"w3-input w3-border w3-round" + (this.state.isPwdValid ? "" : " invalid")}
                          id="signInPwrd"
                          onChange={this.handleFieldChange}
                          onBlur={this.showValidationMsgs}
                          value={this.state.signInPwrd} />
                </p>
                <p className={(this.state.isPwdValid) ? "w3-hide" : "validation-msg"}>{this.state.pwdValidationMsg}</p>
              </div>
            </div>
            <div className="form-controls">
              {/*<button className="sign-in-btn"
                      onClick={this.signIn}
                      disabled={!this.state.canSignIn}>
                Submit
              </button>*/}
              <input  type="submit"
                      value="Submit"
                      className="sign-in-btn"
                      disabled={!this.state.canSignIn}
              />
            </div>
          </form>
        </div>
      </div>
    )
  }
}
const AnimatedSignInView = React.forwardRef((props, ref) => <SignInView innerRef={ref} submit={props.submit} app={props.app}/>)
export {SignIn, SignInView, AnimatedSignInView};
