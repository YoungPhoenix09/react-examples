import React from "react";
import TabPane from "./tabPane";
import DutyPage from "./dutyPage";
import UsersPage from "./usersPage";
import './styles/myRostersDetail.css';

class MyRostersDetail extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    if (this.props.roster) {
      let tabs = [{
        name: "Duties",
        view: (
          <DutyPage roster={this.props.roster}/>
        )
      },
      {
        name: "Users",
        view: (
          <UsersPage roster={this.props.roster}/>
        )
      },
      {
        name: "Schedule",
        view: (
          <div>Schedule Page</div>
        )
      }];

      return (
        <div id="myRostersDetail" className="w3-col s10 w3-container">
          <div className="w3-left-align rosterDtlHdr">
            <span className="rosterDtlNameHdr">Roster Name: </span>
            <span className="rosterDtlTitle">{this.props.roster.name}</span>
          </div>
          <TabPane tabs={tabs}/>
        </div>
      )
    } else {
      return (
        <div id="myRostersDetail" className="w3-col s10">
        </div>
      )
    }
  }
}

export default MyRostersDetail;
