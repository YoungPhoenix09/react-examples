import React from 'react'
import './styles/rowLoadingSpinner.scss'

class RowLoadingSpinner extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div className={(this.props.isShowing) ? "spinner-container" : "w3-hide"}>
        <svg className="lg-dot" height="30" width="30">
          <circle cx="15" cy="5" r="3" stroke="black" strokeWidth="3" fill="black" />
        </svg>
        <svg className="med-dot" height="30" width="30">
          <circle cx="7" cy="17" r="2" stroke="black" strokeWidth="3" fill="black" />
        </svg>
        <svg className="sm-dot" height="30" width="30">
          <circle cx="15" cy="27" r="1" stroke="black" strokeWidth="3" fill="black" />
        </svg>
      </div>

    )
  }
}

export default RowLoadingSpinner
