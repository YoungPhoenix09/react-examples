import React from 'react'

class MsgPane extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidUpdate () {
    const scrollHeight = this.viewNode.scrollHeight;
    const height = this.viewNode.clientHeight;
    const maxScrollTop = scrollHeight - height;
    this.viewNode.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
  }

  render () {
    return (
      <div id="msgView" className="admin-messages-view" ref={(elem) => {this.viewNode = elem}}>
        {(this.props.msgs.length > 0) ? (
          this.props.msgs.map((msgObj) => {
            let dateString = msgObj.dateStamp.toLocaleDateString()
            let timeString = msgObj.dateStamp.toLocaleTimeString()

            return (
              <div className="message-block">
                <div className="message-sender">
                  {msgObj.senderFullName}
                </div>
                <div className="message-body">
                  <span className="message-text">{msgObj.message}</span>
                  <div className="message-date-stamp">
                    <p>{dateString}</p>
                    <p>{timeString}</p>
                  </div>
                </div>
              </div>
            )
          })
        ) : (
          <p className="no-messages">NO MESSAGES</p>
        )}
      </div>
    )
  }
}

export default MsgPane
