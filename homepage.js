import React from 'react'
import FontIcons from './fonts'
import SignUp from './signUp'
import { AnimatedSignInView } from './signIn'
import posed, { PoseGroup } from 'react-pose'
//import './styles/homepage.css'
import './styles/homepage.scss'

class HomePage extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    let opacityTansConfig = {
      enter: {
        opacity: 1,
        transition: {
          duration: 500,
          ease: "easeOut",
          delay: 500
        }
      },
      exit: {
        opacity: 0,
        transition: {
          duration: 400,
          ease: "linear",
          delay: 0
        }
      }
    }

    const FeatureWrapper = posed.div(opacityTansConfig)

    let features = (
      <FeatureWrapper key="featureWrapper" id="homePageFeatures" className="w3-row w3-xlarge">
        <div className="w3-card-4 feature">
          <div className="w3-row feature-header">
            <div className="w3-display-container" style={{height: "100%"}}>
              <span className="w3-display-middle">
                <div className="icon-border">{FontIcons.list}</div>
              </span>
            </div>
          </div>
          <div className="w3-row feature-content">
            <div className="w3-display-container" style={{height: "100%"}}>
              <span className="w3-display-middle primary-dark" style={{width: "85%"}}>Manage assignment of tasks for a social group or an entire industry</span>
            </div>
          </div>
        </div>
        <div className="w3-card-4 feature">
          <div className="w3-row feature-header">
            <div className="w3-display-container" style={{height: "100%"}}>
              <span className="w3-display-middle">
                <div className="icon-border">{FontIcons.notify}</div>
              </span>
            </div>
          </div>
          <div className="w3-row feature-content">
            <div className="w3-display-container" style={{height: "100%"}}>
              <span className="w3-display-middle primary-dark" style={{width: "85%"}}>Send out notices of upcoming tasks to those who are scheduled</span>
            </div>
          </div>
        </div>
        <div className="w3-card-4 feature">
          <div className="w3-row feature-header">
            <div className="w3-display-container" style={{height: "100%"}}>
              <span className="w3-display-middle">
                <div className="icon-border w3-display-container">
                  <div className="div-container w3-display-middle" style={{width: "100%"}}>
                    {FontIcons.user} <span className="primary-dark" style={{fontSize: "1.5vw"}}>/</span> {FontIcons.users}
                  </div>
                </div>
              </span>
            </div>
          </div>
          <div className="w3-row feature-content">
            <div className="w3-display-container" style={{height: "100%"}}>
              <span className="w3-display-middle primary-dark" style={{width: "85%"}}>Create your own roster, or keep up with rosters you've joined</span>
            </div>
          </div>
        </div>
      </FeatureWrapper>
    )
    const PosedSignUp = posed(SignUp)(opacityTansConfig)
    const PosedSignInView = posed(AnimatedSignInView)(opacityTansConfig)

    let signUpView = (<PosedSignUp key="signUp" app={this.props.app} />)
    let mblSignUpView = (<PosedSignUp key="mblSignUp" app={this.props.app} />)
    let signInView = (<PosedSignInView key="signIn" app={this.props.app} />)
    let mblSignInView = (<PosedSignInView key="mblSignIn" app={this.props.app} />)

    let desktopView = features
    let mobileView = mblSignInView

    if (this.props.showSignIn) {
      desktopView = signInView
      mobileView = mblSignInView
    } else if (this.props.showSignUp) {
      desktopView = signUpView
      mobileView = mblSignUpView
    }

    return (
      <div id="homePage" className="w3-container">
        <div id="homePageContent">
          <div id="homePageHeader" className="w3-row w3-xlarge">
              <h1>Task assignment, simplified</h1>
          </div>
          <div id="homePageDivider" className="w3-row w3-xlarge">
            <div className="dividing-line"></div>
            <button className={"sign-up-button" + ((this.props.showSignUp || this.props.showSignIn) ? " w3-hide" : "")}
                    onClick={this.props.app.showSignUp}
            >Register</button>
          </div>
          <div className="desktopHome">
            <PoseGroup animateOnMount={true} flipMove={false}>
              {desktopView}
            </PoseGroup>
          </div>
          <div className="mobileHome">
            <PoseGroup animateOnMount={true} flipMove={false}>
              {mobileView}
            </PoseGroup>
          </div>
        </div>
        <div className="w3-container footer">
          <h4>{FontIcons.copyright} RosterMe 2018</h4>
        </div>
      </div>
    )
  }
}

export default HomePage;
